﻿using Domain.Models.DataBase.U042.S0010;
using FluentValidation;

namespace BusinessLogic.Validations.U042.S0010
{
    /// <summary>
    /// U042S0010.Validator
    /// </summary>
    public class InsertValidationLand0420 : AbstractValidator<Land0420_insert>
    {
        /// <summary>
        /// land0420 驗證
        /// </summary>
        public InsertValidationLand0420()
        {
            this.RuleFor(param => param.Files_no_old)
                .NotEmpty()
                .WithMessage($"{nameof(Land0420_insert.Files_no_old)}必填");
        }
    }
}
