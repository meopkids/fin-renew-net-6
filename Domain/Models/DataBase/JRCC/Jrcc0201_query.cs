﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 區處查詢
    /// </summary>
    public class Jrcc0201_query
    {
        /// <summary>
        /// 區處Id
        /// </summary>
        public string? Division_id { get; set; }

        /// <summary>
        /// 區處名
        /// </summary>
        public string? Division_name { get; set; }
    }
}
