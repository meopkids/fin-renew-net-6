﻿using Domain.Models.DataBase.Enum;

namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 用途別
    /// </summary>
    public class Jrcc0206_get
    {
        /// <summary>
        /// 用途Id
        /// </summary>
        /// <example>010</example>
        public string Use_id { get; set; }

        /// <summary>
        /// 用途名
        /// </summary>
        /// <example>幹線</example>
        public string Use_name { get; set; }

        /// <summary>
        /// 事業/非事業
        /// B: 事業用地, N: 非事業用地
        /// </summary>
        /// <example>B</example>
        public string B_enterprise { get; set; }
    }
}
