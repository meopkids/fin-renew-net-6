﻿using Microsoft.AspNetCore.Http;
using System.Text;

namespace UtilityLib.Extensions
{
    /// <summary>
    /// SessionExtensionsHelper
    /// </summary>
    public static class SessionExtension
    {
        /// <summary>
        /// Model To SessJson
        /// </summary>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, value.ToJson());
        }

        /// <summary>
        /// SessionJson to Model
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T? GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : value.ToModel<T>();
        }
    }
}
