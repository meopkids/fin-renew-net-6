﻿using NLog;
using System.Text;
using UtilityLib.Interfaces.NLog;

namespace UtilityLib.Services.NLog
{
    /// <summary>
    /// NLog
    /// </summary>
    public class NLogService : INLogService
    {
        /// <summary>
        /// NLog
        /// Issue: 在其他Service注入 NLogSerivce，NameSpace 只會顯示 JWLib.Services.NLog，
        /// 導致NLog.config rule 沒辦法發揮作用
        /// </summary>
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 訊息，記錄不影響系統執行的訊息，通常會記錄登入登出或是資料的建立刪除、傳輸等。
        /// </summary>
        /// <param name="message"></param>
        public void InfoLog(string message)
        {
            this._logger.Info(message);
        }

        /// <summary>
        /// 追蹤，紀錄操作歷程等。
        /// </summary>
        /// <param name="message"></param>
        public void TraceLog(string message)
        {
            this._logger.Trace(message);
        }

        /// <summary>
        /// 警告，用於需要提示的訊息，例如庫存不足、貨物超賣、餘額即將不足等。
        /// </summary>
        /// <param name="message"></param>
        public void WarnLog(string message)
        {
            this._logger.Warn(message);
        }

        /// <summary>
        /// 用於開發，於開發時將一些需要特別關注的訊息以Debug傳出。
        /// </summary>
        /// <param name="message"></param>
        public void DebugLog(string message)
        {
            this._logger.Debug(message);
        }

        /// <summary>
        /// 錯誤，記錄系統實行所發生的錯誤，例如資料庫錯誤、遠端連線錯誤、發生例外等。
        /// </summary>
        /// <param name="message"></param>
        public void ErrorLog(string message)
        {
            this._logger.Error(message);
        }

        /// <summary>
        /// 錯誤，記錄系統實行所發生的錯誤，例如資料庫錯誤、遠端連線錯誤、發生例外等。
        /// </summary>
        /// <param name="message"></param>
        public void FatalLog(string message)
        {
            this._logger.Fatal(message);
        }

        /// <summary>
        /// 訊息，記錄不影響系統執行的訊息，通常會記錄登入登出或是資料的建立刪除、傳輸等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        public void InfoLog(string snamespace, Exception exception)
        {
            this._logger.Info(this.CreateContent(snamespace, exception, false));
        }

        /// <summary>
        /// 追蹤，紀錄操作歷程等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        public void TraceLog(string snamespace, Exception exception)
        {
            this._logger.Trace(this.CreateContent(snamespace, exception, false));
        }

        /// <summary>
        /// 警告，用於需要提示的訊息，例如庫存不足、貨物超賣、餘額即將不足等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        public void WarnLog(string snamespace, Exception exception)
        {
            this._logger.Warn(this.CreateContent(snamespace, exception, false));
        }

        /// <summary>
        /// 用於開發，於開發時將一些需要特別關注的訊息以Debug傳出。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        public void DebugLog(string snamespace, Exception exception)
        {
            this._logger.Debug(this.CreateContent(snamespace, exception, false));
        }

        /// <summary>
        /// 錯誤，記錄系統實行所發生的錯誤，例如資料庫錯誤、遠端連線錯誤、發生例外等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        public void ErrorLog(string snamespace, Exception exception)
        {
            this._logger.Error(this.CreateContent(snamespace, exception, false));
        }

        /// <summary>
        /// 錯誤，記錄系統實行所發生的錯誤，例如資料庫錯誤、遠端連線錯誤、發生例外等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        public void FatalLog(string snamespace, Exception exception)
        {
            this._logger.Fatal(this.CreateContent(snamespace, exception, false));
        }

        /// <summary>
        /// CreateContent
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="ex"></param>
        /// <param name="isEmailFormat"></param>
        /// <returns></returns>
        private string CreateContent(string snamespace, Exception ex, bool isEmailFormat)
        {
            Exception logException = ex;
            string htmlLine = string.Empty;

            if (ex?.InnerException != null)
            {
                logException = ex.InnerException;
            }

            if (isEmailFormat)
            {
                htmlLine = "<br>";
            }

            StringBuilder message = new StringBuilder();
            message.AppendLine();
            message.AppendLine("namespace:" + snamespace + $"{htmlLine}");
            message.AppendLine("例外類型:" + logException.GetType().Name + $"{htmlLine}");
            message.AppendLine("例外訊息:" + logException.Message + $"{htmlLine}");
            message.AppendLine("例外來源:" + logException.Source + $"{htmlLine}");
            message.AppendLine("Stack Trace:" + logException.StackTrace + $"{htmlLine}");
            message.AppendLine("TargetSite:" + logException.TargetSite !.ToString() + $"{htmlLine}");

            return message.ToString();
        }
    }
}
