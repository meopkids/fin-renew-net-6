﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace UtilityLib.Extensions
{
    /// <summary>
    /// JsonExtensions
    /// </summary>
    public static class JsonExtension
    {
        /// <summary>
        /// Model轉Json
        /// </summary>
        /// <typeparam name="T">模型</typeparam>
        /// <param name="value"></param>
        /// <param name="limitItem"></param>
        /// <returns></returns>
        public static string ToJson<T>(this T value, string[]? limitItem = null)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                NullValueHandling = NullValueHandling.Ignore //關掉null的項目
            };

            if (limitItem != null)
            {
                settings.ContractResolver = new LimitPropsContractResolver(limitItem, false);
            }

            return JsonConvert.SerializeObject(value, Formatting.Indented, settings);
        }

        /// <summary>
        /// IgnoreModel (過濾不要輸出的屬性)
        /// </summary>
        /// <typeparam name="T">模型</typeparam>
        /// <param name="value"></param>
        /// <param name="ignoreString"></param>
        /// <returns></returns>
        public static T JsonIgnore<T>(this T value, string[]? ignoreString = null)
        {
            return value.ToJson(ignoreString).ToModel<T>();
        }

        /// <summary>
        /// Json轉Model
        /// </summary>
        /// <typeparam name="T">模型</typeparam>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static T ToModel<T>(this string jsonString) => JsonConvert.DeserializeObject<T>(jsonString) !;

        /// <summary>
        /// JObject轉Json
        /// </summary>
        /// <typeparam name="T">模型</typeparam>
        /// <param name="jsonObject"></param>
        /// <returns></returns>
        public static T ToModel<T>(this JObject jsonObject) => jsonObject.ToObject<T>() !;
    }

    /// <summary>
    /// Json類別以英文字母排序
    /// </summary>
    public class OrderedContractResolver : DefaultContractResolver
    {
        /// <summary>
        /// Json類別以英文字母排序
        /// </summary>
        /// <param name="type"></param>
        /// <param name="memberSerialization"></param>
        /// <returns></returns>
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return base.CreateProperties(type, memberSerialization).OrderBy(p => p.PropertyName).ToList();
        }
    }

    /// <summary>
    /// LimitPropsContractResolver
    /// </summary>
    public class LimitPropsContractResolver : DefaultContractResolver
    {
        /// <summary>
        /// 屬性
        /// </summary>
        private string[] props = null!;

        /// <summary>
        /// 值
        /// </summary>
        private bool retain;

        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="props">傳入的屬性數組</param>
        /// <param name="retain">true:表示props是需要保留的字段  false：表示props是要排除的字段</param>
        public LimitPropsContractResolver(string[] props, bool retain = true)
        {
            //指定要序列化屬性的清單
            this.props = props;
            this.retain = retain;
        }

        /// <summary>
        /// CreateProperties
        /// </summary>
        /// <param name="type"></param>
        /// <param name="memberSerialization"></param>
        /// <returns></returns>
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            IList<JsonProperty> list = base.CreateProperties(type, memberSerialization);
            //只保留清單有列出的屬性
            return list.Where(p =>
            {
                if (this.retain)
                {
                    return this.props.Contains(p.PropertyName);
                }
                else
                {
                    return !this.props.Contains(p.PropertyName);
                }
            }).ToList();
        }
    }
}
