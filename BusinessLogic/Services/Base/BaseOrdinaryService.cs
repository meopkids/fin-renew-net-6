﻿using DataAccess.Interfaces;
using Domain.Models.Security;
using Microsoft.AspNetCore.Http;
using UtilityLib.Extensions;

namespace BusinessLogic.Services.Base
{
    /// <summary>
    /// 一般的基礎服務類別
    /// </summary>
    public class BaseOrdinaryService
    {
        /// <summary>
        /// IDapperService
        /// </summary>
        protected internal IDapperService DapperService { get; set; }

        /// <summary>
        /// HttpContextAccessor存取
        /// </summary>
        protected IHttpContextAccessor HttpContextAccessor { get; set; }

        /// <summary>
        /// Session
        /// </summary>
        protected ISession Session => this.HttpContextAccessor.HttpContext !.Session;

        /// <summary>
        /// Dapper資料庫控制服務
        /// </summary>
        protected IDapperRepository DapperRepository { get; set; }

        /// <summary>
        /// Request是否來自Swagger
        /// </summary>
        protected bool IsSwagger => !string.IsNullOrEmpty(this.HttpContextAccessor.HttpContext!.Request.Headers["swagger-ui"]);

        /// <summary>
        /// 使用者資訊
        /// </summary>
        protected Payload? Payload => this.Session.GetObjectFromJson<Payload>("user_context");

        /// <summary>
        /// 查詢回傳資料筆數上限: TOP(int X)
        /// </summary>
        /// <returns></returns>
        protected string RecordLIMTD => this.IsSwagger.Bool() ? " TOP(10)" : "";
    }
}
