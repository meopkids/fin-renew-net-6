﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models.General
{
    /// <summary>
    /// 工作站與區處
    /// </summary>
    public class DivisionUnit
    {
        /// <summary>
        /// 區處
        /// </summary>
        [Required]
        public string Division_id { get; set; }

        /// <summary>
        /// 工作站
        /// </summary>
        [Required]
        public string Unit_id { get; set; }
    }
}
