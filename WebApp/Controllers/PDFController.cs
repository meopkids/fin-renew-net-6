﻿using Microsoft.AspNetCore.Mvc;
using SelectPdf;

namespace WebApp.Controllers
{
    /// <summary>
    /// PDF路由
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PDFController : ControllerBase
    {
        /// <summary>
        /// Test
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Test()
        {
            string url = "https://selectpdf.com";
            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

            PdfDocument doc = converter.ConvertUrl(url);
            byte[] byteArray = doc.Save();
            MemoryStream ms = new MemoryStream(byteArray);
            ms.Write(byteArray, 0, byteArray.Length);
            ms.Position = 0;
            doc.Close();
            return new FileStreamResult(ms, "application/pdf");
        }
    }
}
