﻿namespace Domain.Models.Security
{
    /// <summary>
    /// 驗證結果
    /// </summary>
    public class AuthenticationResult
    {
        /// <summary>
        /// Status
        /// </summary>
        public ValidType Status { get; set; }

        /// <summary>
        /// Payload
        /// </summary>
        public Payload UserInfo { get; set; }
    }
}
