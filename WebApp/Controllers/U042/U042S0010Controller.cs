﻿using BusinessLogic.Attributes;
using BusinessLogic.Interfaces;
using BusinessLogic.Services.U042;
using Domain.Models.DataBase.U042.S0010;
using Domain.Models.General;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.U042
{
    /// <summary>
    /// 土地占用
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [TypeFilter(typeof(CustomAuthorizeAttribute))]
    public class U042S0010Controller : ControllerBase
    {
        #region 引入服務

        /// <summary>
        /// 狀態碼管理服務
        /// </summary>
        private readonly IHTTPStatusWrap _wrapper;

        /// <summary>
        /// 占用資料管理
        /// </summary>
        private readonly U042S0010Service _u042s0010;

        #endregion

        /// <summary>
        /// 注入/初始化
        /// </summary>
        /// <param name="httpWrapper"></param>
        /// <param name="s0010"></param>
        public U042S0010Controller(IHTTPStatusWrap httpWrapper, U042S0010Service s0010)
        {
            this._u042s0010 = s0010;
            this._wrapper = httpWrapper;
        }

        /// <summary>
        /// 占用資料管理 列表
        /// </summary>
        /// <param name="query">占用資料管理Api 查詢參數</param>
        /// <returns></returns>
        /// <response code="200">回傳占用列表資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("list")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<U042S0010_list>), 200)]
        public IActionResult Get([FromQuery] U042S0010_search query)
        {
            return this._wrapper.UniResponse(this._u042s0010.GetListData(query));
        }

        /// <summary>
        /// 占用主檔 land0420
        /// </summary>
        /// <param name="id">land0420 占用主檔Id</param>
        /// <returns></returns>
        /// <response code="200">回傳占用主檔資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("{id:Guid}")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Land0420_get>), 200)]
        public IActionResult Get(Guid id)
        {
            return this._wrapper.UniResponse(this._u042s0010.GetLand0420Data(id));
        }

        /// <summary>
        /// 新增資料 land0420
        /// </summary>
        /// <param name="data">占用主檔資料</param>
        /// <returns></returns>
        /// <response code="200">新增成功</response>
        /// <response code="400">發生錯誤</response>
        [HttpPost]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(GeneralResponse), 200)]
        public IActionResult Post([FromForm] Land0420_insert data)
        {
            return this._wrapper.UniResponse(this._u042s0010.InsertMainData(data));
        }

        /// <summary>
        /// 更新資料 land0420
        /// </summary>
        /// <param name="id">land0420 占用主檔Id: 8DBE40F2-BA77-4A73-8838-0037C17BACF3</param>
        /// <param name="data"></param>
        /// <returns></returns>
        /// <response code="200">已更新資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpPut]
        [Route("{id:Guid}")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(GeneralResponse), 200)]
        public IActionResult Put(Guid id, [FromForm] Land0420_update data)
        {
            return this._wrapper.UniResponse(this._u042s0010.PutMainData(id, data));
        }

        /// <summary>
        /// 刪除資料 land0420
        /// </summary>
        /// <param name="id">land0420 占用主檔Id</param>
        /// <returns></returns>
        /// <response code="200">刪除成功</response>
        /// <response code="400">發生錯誤</response>
        [HttpDelete]
        [Route("{id:Guid}")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(GeneralResponse), 200)]
        public IActionResult Delete(Guid id)
        {
            return this._wrapper.UniResponse(this._u042s0010.DeleteMainData(id));
        }

        /// <summary>
        /// 占用調查明細 land0421
        /// </summary>
        /// <param name="oid">land0420 占用主檔Id</param>
        /// <returns></returns>
        /// <response code="200">回傳占用調查明細資料</response>
        /// <response code="400">找不到資料</response>
        [HttpGet]
        [Route("navigation/{oid:Guid}")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Land0421_get>), 200)]
        public IActionResult GetNavi(Guid oid)
        {
            return this._wrapper.UniResponse(this._u042s0010.GetNavigData(oid));
        }

        /// <summary>
        /// 新增資料 land0421
        /// </summary>
        /// <param name="data">占用調查明細資料</param>
        /// <returns></returns>
        /// <response code="200">新增成功</response>
        /// <response code="400">發生錯誤</response>
        [HttpPost]
        [Route("navigation")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(GeneralResponse), 200)]
        public IActionResult PostNavi([FromForm] Land0421_insert data)
        {
            return this._wrapper.UniResponse(this._u042s0010.InsertNavigData(data));
        }

        /// <summary>
        /// 更新資料 land0421
        /// </summary>
        /// <param name="id">land0421 占用調查Id</param>
        /// <param name="data">占用調查明細資料</param>
        /// <returns></returns>
        /// <response code="200">新增成功</response>
        /// <response code="400">發生錯誤</response>
        [HttpPut]
        [Route("navigation/{id:Guid}")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(GeneralResponse), 200)]
        public IActionResult PutNavi(Guid id, [FromForm] Land0421_update data)
        {
            return this._wrapper.UniResponse(this._u042s0010.PutNaviData(id, data));
        }

        /// <summary>
        /// 刪除資料 land0421
        /// </summary>
        /// <param name="id">land0421 占用調查Id</param>
        /// <returns></returns>
        /// <response code="200">刪除成功</response>
        /// <response code="400">發生錯誤</response>
        [HttpDelete]
        [Route("navigation/{id:Guid}")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(GeneralResponse), 200)]
        public IActionResult DeleteNavi(Guid id)
        {
            return this._wrapper.UniResponse(this._u042s0010.DeleteNavigData(id));
        }

        // TODO: 使用補償金，先整理各繳款模組成一個整合模組
    }
}
