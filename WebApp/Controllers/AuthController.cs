﻿using BusinessLogic.Services;
using Domain.Models.Account;
using Domain.Models.Security;
using Microsoft.AspNetCore.Mvc;
using UtilityLib.Extensions;

namespace WebApp.Controllers
{
    /// <summary>
    /// 帳號登入、Token授權
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        #region 引入服務
        // 安全令牌服務
        private readonly AccountManagerService _account;
        #endregion

        #region 注入/初始化

        /// <summary>
        /// 注入/初始化
        /// </summary>
        /// <param name="accountManager"></param>
        public AuthController(AccountManagerService accountManager)
        {
            this._account = accountManager;
        }
        #endregion

        #region 公開方法

        /// <summary>
        /// 產生Token
        /// </summary>
        /// <param name="logininfo">登入資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳Token</response>
        /// <response code="400">錯誤的登入資訊</response>
        [HttpPost]
        [Route("Token")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(Token), 200)]
        public IActionResult GenerateToken([FromForm] LoginModel logininfo) // 雖然ApiController已經預設為FromForm，但為了 Swagger-UI 產生input欄位還是得標注
        {
            User? userInfo = this._account.Identify(logininfo);

            if (userInfo is null)
            {
                return this.NotFound();
            }

            return this.Ok(this._account.GetToken(userInfo));
        }
        #endregion
    }
}
