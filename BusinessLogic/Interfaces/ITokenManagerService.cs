﻿using Domain.Models.Security;

namespace BusinessLogic.Interfaces
{
    /// <summary>
    /// Token介面
    /// </summary>
    public interface ITokenManagerService
    {
        /// <summary>
        /// Create Token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Token Create(User user);

        /// <summary>
        /// Verify Token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        AuthenticationResult Verify(string token);
    }
}
