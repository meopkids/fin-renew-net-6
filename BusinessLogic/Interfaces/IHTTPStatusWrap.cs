﻿using Domain.Models.General;
using Microsoft.AspNetCore.Mvc;

namespace BusinessLogic.Interfaces
{
    /// <summary>
    /// 狀態碼管理介面
    /// </summary>
    public interface IHTTPStatusWrap
    {
        /// <summary>
        /// 回復方法
        /// </summary>
        /// <param name="general"></param>
        /// <returns></returns>
        IActionResult UniResponse(GeneralResponse general);
    }
}
