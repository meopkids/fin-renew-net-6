﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models.DataBase.U042.S0010
{
    /// <summary>
    /// land0420 新增資料
    /// </summary>
    public class Land0420_insert
    {
        /// <summary>
        /// 檔案編號
        /// </summary>
        /// <example>104120008</example>
        [Required]
        public string Files_no { get; set; }

        /// <summary>
        /// 舊檔案編號
        /// </summary>
        /// <example>1718</example>
        public string? Files_no_old { get; set; }

        /// <summary>
        /// 用途別 ref jrcc0206
        /// </summary>
        /// <example>240</example>
        public string? Occupy_type { get; set; }

        /// <summary>
        /// 池塘或土地占用
        /// L: 土地, P: 池塘
        /// </summary>
        /// <example>L</example>
        public string? Types { get; set; }

        /// <summary>
        /// 縣市 ref jrcc0102
        /// </summary>
        /// <example>F</example>
        [Required]
        public string City_id { get; set; }

        /// <summary>
        /// 鄉鎮 ref jrcc0103
        /// </summary>
        /// <example>18</example>
        [Required]
        public string Town_id { get; set; }

        /// <summary>
        /// 地段 ref jrcc0104
        /// </summary>
        /// <example>1859</example>
        [Required]
        public string Sec_id { get; set; }

        /// <summary>
        /// 母號
        /// </summary>
        /// <example>0328</example>
        [Required]
        public string Land_no1 { get; set; }

        /// <summary>
        /// 子號
        /// </summary>
        /// <example>0001</example>
        [Required]
        public string Land_no2 { get; set; }

        /// <summary>
        /// 地目代碼 ref jrcc0106
        /// </summary>
        /// <example>E</example>
        public string? Land_category_id { get; set; }

        /// <summary>
        /// 等則 ref jrcc0301
        /// </summary>
        /// <example>06</example>
        public string? Land_kind { get; set; }

        /// <summary>
        /// 占用面積
        /// </summary>
        /// <example>10.95</example>
        public double Land_area { get; set; }

        /// <summary>
        /// 權利範圍類別
        /// </summary>
        /// <example>A</example>
        public string? P_bound_id { get; set; }

        /// <summary>
        /// 使用分區代碼 ref jrcc0107
        /// </summary>
        /// <example>BA</example>
        public string? Land_use_id { get; set; }

        /// <summary>
        /// 使用地類別代碼 ref jrcc0108
        /// </summary>
        public string? Land_classification_id { get; set; }

        /// <summary>
        /// 區處 ref jrcc0201
        /// </summary>
        /// <example>99</example>
        [Required]
        public string Division_id { get; set; }

        /// <summary>
        /// 工作站 ref jrcc0202
        /// </summary>
        /// <example>12</example>
        [Required]
        public string Unit_id { get; set; }
    }
}
