﻿// yyyMMdd => yyy-MM-dd
$(document).ready(function () {
    let pay_date_end = $('span.roc_date')[0].innerText;

    if (pay_date_end.length == 7) {
        $('span.roc_date')[0].innerText = pay_date_end.substr(0, 3) + '-' + pay_date_end.substr(3, 2) + '-' + pay_date_end.substr(5, 2);
    }
});
