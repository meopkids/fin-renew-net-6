﻿namespace Domain.Models.DataBase.U042.S0010
{
    /// <summary>
    /// 占用調查記錄
    /// </summary>
    public class Land0421_get
    {
        /// <summary>
        /// land0421 主鍵
        /// </summary>
        /// <example>4b50589d-f121-450c-91e5-d9daa04f2536</example>
        public Guid Id { get; set; }

        /// <summary>
        /// land0421 資料外鍵 參考land0420主鍵id
        /// </summary>
        /// <example>8dbe40f2-ba77-4a73-8838-0037c17bacf3</example>
        public Guid Oid { get; set; }

        /// <summary>
        /// If_unjust_enrichment
        /// </summary>
        /// <example>Y</example>
        public string If_unjust_enrichment { get; set; }

        /// <summary>
        /// 素地
        /// </summary>
        /// <example>N</example>
        public string If_vegetarian { get; set; }

        /// <summary>
        /// If_closing
        /// </summary>
        /// <example>A</example>
        public string If_closing { get; set; }

        /// <summary>
        /// 占用面積
        /// </summary>
        /// <example>5.34</example>
        public double Occupy_area { get; set; }

        /// <summary>
        /// 占用期間起
        /// </summary>
        /// <example>0990707</example>
        public string Occupy_period_start { get; set; }

        /// <summary>
        /// 占用期間終
        /// </summary>
        /// <example>1040707</example>
        public string Occupy_period_end { get; set; }

        /// <summary>
        /// Land_value_math
        /// </summary>
        /// <example>19840</example>
        public string Land_value_math { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        /// <example>8</example>
        public double Rate { get; set; }

        /// <summary>
        /// Rate_pre
        /// </summary>
        public string Rate_pre { get; set; }

        /// <summary>
        /// Land_no1_1
        /// </summary>
        /// <example>0311</example>
        public string Land_no1_1 { get; set; }

        /// <summary>
        /// Land_no2_1
        /// </summary>
        /// <example>0000</example>
        public string Land_no2_1 { get; set; }

        /// <summary>
        /// Land_no1_2
        /// </summary>
        /// <example>0000</example>
        public string Land_no1_2 { get; set; }

        /// <summary>
        /// Land_no2_2
        /// </summary>
        /// <example>0000</example>
        public string Land_no2_2 { get; set; }

        /// <summary>
        /// View_point
        /// </summary>
        public string View_point { get; set; }

        /// <summary>
        /// Case_id
        /// </summary>
        /// <example>A</example>
        public string Case_id { get; set; }

        /// <summary>
        /// Irrigation_ditch_type
        /// </summary>
        /// <example>A</example>
        public string Irrigation_ditch_type { get; set; }

        /// <summary>
        /// Irrigation_ditch_id
        /// </summary>
        /// <example>永豐圳西支線</example>
        public string Irrigation_ditch_id { get; set; }

        /// <summary>
        /// Irrigation_ditch_name
        /// </summary>
        /// <example>西小組分線</example>
        public string Irrigation_ditch_name { get; set; }

        /// <summary>
        /// Location_X
        /// </summary>
        public string Location_X { get; set; }

        /// <summary>
        /// Location_Y
        /// </summary>
        public string Location_Y { get; set; }

        /// <summary>
        /// Summarize
        /// </summary>
        /// <example>磚造房屋自用</example>
        public string Summarize { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        /// <example>林芳綺</example>
        public string Name { get; set; }

        /// <summary>
        /// Name_check
        /// </summary>
        /// <example>N</example>
        public string Name_check { get; set; }

        /// <summary>
        /// Name_check1
        /// </summary>
        /// <example>N</example>
        public string Name_check1 { get; set; }

        /// <summary>
        /// Idcard_no
        /// </summary>
        /// <example>L222133804</example>
        public string Idcard_no { get; set; }

        /// <summary>
        /// Telephone
        /// </summary>
        /// <example>228977033、0936068416</example>
        public string Telephone { get; set; }

        /// <summary>
        /// Post_code
        /// </summary>
        /// <example>新北市中和區景德街89巷3號</example>
        public string Post_code { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Investigator
        /// </summary>
        /// <example>邱建中</example>
        public string Investigator { get; set; }

        /// <summary>
        /// Investigate_date
        /// </summary>
        /// <example>1040707</example>
        public string Investigate_date { get; set; }

        /// <summary>
        /// Investigate_result
        /// </summary>
        /// <example>該建物共占用本會三筆土地</example>
        public string Investigate_result { get; set; }

        /// <summary>
        /// Clamp_down_result
        /// </summary>
        public string Clamp_down_result { get; set; }

        /// <summary>
        /// Check_date
        /// </summary>
        /// <example>1040924</example>
        public string Check_date { get; set; }

        /// <summary>
        /// Processor_id
        /// </summary>
        /// <example>D</example>
        public string Processor_id { get; set; }

        /// <summary>
        /// Processor_opinion
        /// </summary>
        public string Processor_opinion { get; set; }

        /// <summary>
        /// Processor_way
        /// </summary>
        /// <example>收取五年占用補償金後辦理出租</example>
        public string Processor_way { get; set; }

        /// <summary>
        /// Water_processor_advise
        /// </summary>
        public string Water_processor_advise { get; set; }

        /// <summary>
        /// Recycle_date
        /// </summary>
        public string Recycle_date { get; set; }

        /// <summary>
        /// Recycle_record
        /// </summary>
        public string Recycle_record { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Objection_note
        /// </summary>
        public string Objection_note { get; set; }

        /// <summary>
        /// Review_results
        /// </summary>
        public string Review_results { get; set; }

        /// <summary>
        /// M_update_id
        /// </summary>
        /// <example>a272</example>
        public string M_update_id { get; set; }

        /// <summary>
        /// M_update_date
        /// </summary>
        /// <example>1040924</example>
        public string M_update_date { get; set; }

        /// <summary>
        /// S_update_id
        /// </summary>
        /// <example>a269</example>
        public string S_update_id { get; set; }

        /// <summary>
        /// S_update_date
        /// </summary>
        /// <example>1040924</example>
        public string S_update_date { get; set; }

        /// <summary>
        /// L_update_id
        /// </summary>
        /// <example>a077</example>
        public string L_update_id { get; set; }

        /// <summary>
        /// L_update_date
        /// </summary>
        /// <example>1041124</example>
        public string L_update_date { get; set; }

        /// <summary>
        /// 負責人
        /// </summary>
        /// <example>a077</example>
        public string Handle_id { get; set; }

        /// <summary>
        /// 異動人
        /// </summary>
        /// <example>a077</example>
        public string Update_id { get; set; }

        /// <summary>
        /// 異動日期
        /// </summary>
        /// <example>1041124</example>
        public string Update_date { get; set; }

        /// <summary>
        /// 業務負責人
        /// </summary>
        /// <example>張樂燕</example>
        public string Uname { get; set; }
    }
}
