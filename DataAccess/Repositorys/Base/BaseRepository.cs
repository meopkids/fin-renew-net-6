﻿using DataAccess.Interfaces;
using Domain.Caches;
using Microsoft.Extensions.Configuration;

namespace DataAccess.Repositorys.Base
{
    /// <summary>
    /// 資料庫基礎共用
    /// </summary>
    public class BaseRepository
    {
        /// <summary>
        /// Configuration
        /// </summary>
        protected internal IConfiguration Configuration; // => new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("AppSettings.json").Build();

        /// <summary>
        /// DapperService
        /// </summary>
        protected internal IDapperService DapperService1;

        /// <summary>
        /// SQL connection
        /// </summary>
        protected internal ConnectionStrings ConnectionStrings;

        /// <summary>
        /// 連線字串
        /// </summary>
        protected internal string SqlCommand;
    }
}
