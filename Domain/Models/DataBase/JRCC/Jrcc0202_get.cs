﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 工作站
    /// </summary>
    public class Jrcc0202_get
    {
        /// <summary>
        /// 工作站Id
        /// </summary>
        /// <example>0040</example>
        public string Unit_id { get; set; }

        /// <summary>
        /// 區處Id
        /// </summary>
        /// <example>00</example>
        public string Division_id { get; set; }

        /// <summary>
        /// 工作站名
        /// </summary>
        /// <example>管理組</example>
        public string Unit_name { get; set; }

        /// <summary>
        /// 類別
        /// </summary>
        /// <example>A</example>
        public string C_type { get; set; }
    }
}
