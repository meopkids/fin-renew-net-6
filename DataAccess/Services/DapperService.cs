﻿using Dapper;
using DataAccess.Interfaces;
using DataAccess.Models;
using System.Data.SqlClient;

namespace DataAccess.Services
{
    /// <summary>
    /// Dapper 基礎設施
    /// </summary>
    public class DapperService : IDapperService
    {
        /// <summary>
        /// 執行命令
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Execute(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            return conn.Execute(model.SqlCommand, model.SqlParamData);
        }

        /// <summary>
        /// 執行命令(非同步)
        /// </summary>
        /// <param name="model"></param>
        /// <returns>A Task type representing the number of rows affected asynchronously.</returns>
        public async Task<int> ExecuteAsync(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            return await conn.ExecuteAsync(model.SqlCommand, model.SqlParamData);
        }

        /// <summary>
        /// 全部回傳
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<T> Query<T>(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            return conn.Query<T>(model.SqlCommand, model.SqlParamData).ToList();
        }

        /// <summary>
        /// 全部回傳(非同步)
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<List<T>> QueryAsync<T>(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            IEnumerable<T> test = await conn.QueryAsync<T>(model.SqlCommand, model.SqlParamData);
            return test.ToList();
        }

        /// <summary>
        /// 會將符合條件的第一筆回傳回來，如果沒有符合回傳null。
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public T QueryFirstOrDefault<T>(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            return conn.QueryFirstOrDefault<T>(model.SqlCommand, model.SqlParamData);
        }

        /// <summary>
        /// 會將符合條件的第一筆回傳回來，如果沒有符合回傳null。(非同步)
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<T> QueryFirstOrDefaultAsync<T>(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            return await conn.QueryFirstOrDefaultAsync<T>(model.SqlCommand, model.SqlParamData);
        }

        /// <summary>
        /// 查詢唯一符合條件的資料，如果沒有符合或符合條件為多筆時會拋出錯誤。
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public T QuerySingle<T>(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            return conn.QuerySingle<T>(model.SqlCommand, model.SqlParamData);
        }

        /// <summary>
        /// 查詢唯一符合條件的資料，如果沒有符合或符合條件為多筆時會拋出錯誤。(非同步)
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<T> QuerySingleAsync<T>(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            return await conn.QuerySingleAsync<T>(model.SqlCommand, model.SqlParamData);
        }

        /// <summary>
        /// 查詢唯一符合條件的資料，如果沒有符合回傳null，但如果符合條件為多筆時會拋出錯誤。
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public T QuerySingleOrDefault<T>(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            return conn.QuerySingleOrDefault<T>(model.SqlCommand, model.SqlParamData);
        }

        /// <summary>
        /// 查詢唯一符合條件的資料，如果沒有符合回傳null，但如果符合條件為多筆時會拋出錯誤。(非同步)
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<T> QuerySingleOrDefaultAsync<T>(DapperConnectModel model)
        {
            using SqlConnection conn = new SqlConnection(model.SqlConnectionString);
            return await conn.QuerySingleOrDefaultAsync<T>(model.SqlCommand, model.SqlParamData);
        }
    }
}
