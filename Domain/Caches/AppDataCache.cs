﻿namespace Domain.Caches
{
    /// <summary>
    /// AppCatch
    /// </summary>
    public static class AppDataCache
    {
        /// <summary>
        /// 資料庫連接字串
        /// </summary>
        public static string SqlConnect { get; set; }

        /// <summary>
        /// APP加密金鑰
        /// </summary>
        public static string AppEncryptKey { get; set; }
    }
}
