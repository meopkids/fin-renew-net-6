﻿namespace UtilityLib.Extensions
{
    /// <summary>
    /// 布林判斷
    /// </summary>
    public static class BooleanJudgementExtension
    {
        /// <summary>
        /// 檢查參數是否有值或者為不為零，是則返回真，否則返回假
        /// </summary>
        /// <param name="inpObj"></param>
        /// <returns></returns>
        public static bool Bool(this object? inpObj)
        {
            if (inpObj == null)
            {
                return false;
            }

            Type unknown = inpObj.GetType();
            switch (Type.GetTypeCode(unknown))
            {
                case TypeCode.String:
                    return (string)inpObj == "" ? false : true;

                case TypeCode.Int32:
                    if (unknown.IsEnum)
                    {
                        return Bool(inpObj is not null);
                    }

                    return (int)inpObj == 0 ? false : true;

                case TypeCode.Double:
                    return (double)inpObj == 0 ? false : true;

                case TypeCode.Boolean:
                    return (bool)inpObj;

                case TypeCode.Object:
                    return inpObj == null ? false : true;

                default:
                    return false;
            }
        }
    }
}
