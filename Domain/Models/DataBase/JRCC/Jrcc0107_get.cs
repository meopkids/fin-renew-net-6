﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 使用分區別
    /// </summary>
    public class Jrcc0107_get
    {
        /// <summary>
        /// 使用分區Id
        /// </summary>
        /// <example>AA</example>
        public string Land_use_id { get; set; }

        /// <summary>
        /// 使用分區名
        /// </summary>
        /// <example>特定農業區</example>
        public string Land_use_name { get; set; }
    }
}
