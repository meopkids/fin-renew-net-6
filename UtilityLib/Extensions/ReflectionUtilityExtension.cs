﻿using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Linq.Expressions;
using System.Reflection;

namespace UtilityLib.Extensions
{
    /// <summary>
    /// 物件反射擴充方法
    /// </summary>
    public static class ReflectionUtilityExtension
    {
        /// <summary>
        /// 反射舊物件到新物件
        /// </summary>
        /// <typeparam name="T">新物件模型</typeparam>
        /// <param name="inpObj">來源物件</param>
        /// <param name="outObj">新物件</param>
        /// <returns></returns>
        public static T AssignData<T>(this object inpObj, T outObj)
        {
            Type type = inpObj.GetType();
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                SetValue(outObj, property.Name, GetValue(inpObj, property.Name) !);
            }

            return outObj;
        }

        /// <summary>
        /// 反射舊物件到新物件(可輸入排除項目)
        /// </summary>
        /// <typeparam name="T">新物件模型</typeparam>
        /// <param name="inpObj">來源物件</param>
        /// <param name="outObj">新物件</param>
        /// <param name="exclude">不反射的物件名稱</param>
        /// <returns>回傳物件</returns>
        public static T AssignData<T>(this object inpObj, T outObj, List<string> exclude)
        {
            Type type = inpObj.GetType();
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                if (exclude.Contains(property.Name))
                {
                    continue;
                }

                SetValue(outObj, property.Name, GetValue(inpObj, property.Name) !);
            }

            return outObj;
        }

        /// <summary>
        /// 使用Type執行物件方法
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="obj">目標物件</param>
        /// <param name="methodName">要執行的物件方法名稱</param>
        public static void ExecMethod<T>(this T obj, string methodName)
        {
            Type type = obj!.GetType();
            var method = type.GetMethod(methodName);
            if (method != null)
            {
                method.Invoke(obj, null);
            }
        }

        /// <summary>
        /// 取得特定屬性(Property)的屬性名稱(PropertyName String)
        /// 用法: ReflectionUtilityExtension.GetPropertyName(() => account.Password);
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            MemberExpression body = (MemberExpression)expression.Body;
            return body.Member.Name;
        }

        /// <summary>
        /// 取得Object物件中的所有屬性名稱
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="inpObj">Object</param>
        /// <returns>物件的所有屬性名稱</returns>
        public static List<string> GetAllPropertyName<T>(this T inpObj)
        {
            List<string> resualtList = new List<string>();

            Type type = inpObj!.GetType();
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                resualtList.Add(property.Name);
            }

            return resualtList;
        }

        /// <summary>
        /// 設定值到Object屬性
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="obj">Object</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <param name="value">參數值</param>
        public static void SetValue<T>(this T obj, string propertyName, object value)
        {
            Type type = obj!.GetType();
            var property = type.GetProperty(propertyName);
            if (property != null && value is not null)
            {
                property.SetValue(obj, Convert.ChangeType(value, property.PropertyType));
            }
        }

        /// <summary>
        /// 取得Object屬性內的值
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static object? GetValue<T>(this T obj, string propertyName)
        {
            Type type = obj!.GetType();
            var property = type.GetProperty(propertyName);
            if (property != null)
            {
                return property.GetValue(obj) !;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 設定不要反射的屬性名稱
        /// </summary>
        /// <param name="excludeName">屬性名稱清單，用逗號隔開</param>
        /// <returns></returns>
        public static List<string> ReflectionExclude(string excludeName)
        {
            return new List<string>(excludeName.Split(','));
        }
    }
}
