﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 縣市別查詢
    /// </summary>
    public class Jrcc0102_query
    {
        /// <summary>
        /// 縣市Id
        /// </summary>
        public string? City_id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string? City_name { get; set; }
    }
}
