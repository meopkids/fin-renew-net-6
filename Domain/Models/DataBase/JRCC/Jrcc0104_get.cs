﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 地段
    /// </summary>
    public class Jrcc0104_get
    {
        /// <summary>
        /// 縣市Id
        /// </summary>
        /// <example>H</example>
        public string? City_id { get; set; }

        /// <summary>
        /// 鄉鎮Id
        /// </summary>
        /// <example>01</example>
        public string? Town_id { get; set; }

        /// <summary>
        /// 地段Id
        /// </summary>
        /// <example>0001</example>
        public string? Sec_id { get; set; }

        /// <summary>
        /// 段名
        /// </summary>
        /// <example>桃園</example>
        public string? Sec_name { get; set; }

        /// <summary>
        /// 小段
        /// </summary>
        /// <example>武陵</example>
        public string? Sec_seq { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        /// <example>01</example>
        public string? Note { get; set; }

        /// <summary>
        /// 事務所代碼
        /// </summary>
        /// <example>01</example>
        public string? Llot_id { get; set; }
    }
}
