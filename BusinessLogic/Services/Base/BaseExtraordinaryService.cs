﻿using System.Text.RegularExpressions;
using UtilityLib.Extensions;

namespace BusinessLogic.Services.Base
{
    /// <summary>
    /// 很棒的基底服務類別，繼承: 一般的基礎服務類別
    /// </summary>
    public class BaseExtraordinaryService : BaseOrdinaryService
    {
        /// <summary>
        /// 如果為Swagger UI request，回傳限制筆數的SQL statement
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <returns></returns>
        protected string RowLIMTD(string sqlCmd)
        {
            // Find The Last Block of "SELECT * FROM"
            MatchCollection matches_0 = new Regex(
                @"(?<=\n\s*SELECT)", // Positive Lookbehind With (newline) (space)* SELECT
                RegexOptions.IgnoreCase) // Case Insensitive
                .Matches(sqlCmd);
            if (matches_0.Count > 0)
            {
                int insertIndex = matches_0[matches_0.Count - 1].Index;
                return sqlCmd.Insert(insertIndex, this.RecordLIMTD);
            }

            // Find SELECT In First Layer
            MatchCollection matches_1 = new Regex(@"(?<=SELECT)|FROM", RegexOptions.IgnoreCase).Matches(sqlCmd);
            if (matches_1.Count > 0)
            {
                // TODO: To Consider Any Condition Like "SELECT" Isn't Couple With "FROM"
                int insertIndex = matches_1[0].Index;
                return sqlCmd.Insert(insertIndex, this.RecordLIMTD);
            }

            return sqlCmd;
        }

        /// <summary>
        /// 從物件取得非NULL的屬性，轉成SqlParam字串，並且判斷銜尾詞賦予相應的SQL比較運算元
        /// <para>1. default： =</para>
        /// <para>2. _start： ≧</para>
        /// <para>3. _end： ≦</para>
        /// <para>4. _name： like</para>
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="model"></param>
        /// <param name="manifest"></param>
        /// <param name="tableName"></param>
        /// <returns>(<see cref="string"/>) ' and PropertyName = @PropertyName, ... '</returns>
        protected string AttributeToWhereClause<T>(T model, string[] manifest, string? tableName = "")
        {
            List<string> ret = new List<string>();
            List<string> properties = model
                    .GetAllPropertyName()
                   .AsEnumerable() // Language Integrated Query(Linq) is based on Enumerable Type
                  .Where(x =>
                    model.GetValue(x) != null && manifest.Contains(x))
                .ToList();

            if (properties.Count == 0)
            {
                return string.Empty;
            }

            Regex startRegex = new Regex(@"_start$");
            Regex endRegex = new Regex(@"_end$");
            Regex nameRegex = new Regex(@"name$");
            string prefix = string.IsNullOrEmpty(tableName) ? "" : tableName + ".";

            foreach (string name in properties)
            {
                switch (name)
                {
                    case string n when startRegex.IsMatch(n) == true:
                        ret.Add($"and {prefix + name}>=@{name}");
                        break;
                    case string n when endRegex.IsMatch(n) == true:
                        ret.Add($"and {prefix + name}<=@{name}");
                        break;
                    case string n when nameRegex.IsMatch(n.ToLower()) == true:
                        ret.Add($"and {prefix + name} like '%' + @{name} + '%'");
                        break;
                    default:
                        ret.Add($"and {prefix + name}=@{name}");
                        break;
                }
            }

            return string.Join(" ", ret) + " ";
        }

        /// <summary>
        /// 從物件取得非NULL的屬性，轉成SqlParam字串
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="model"></param>
        /// <param name="manifest"></param>
        /// <returns>(<see cref="string"/>) and PropertyName like %@PropertyName%, ...</returns>
        protected string AttributeToPatternClause<T>(T model, string[] manifest)
        {
            List<string> ret = new List<string>();
            List<string> properties = model
                    .GetAllPropertyName()
                   .AsEnumerable()
                  .Where(x =>
                    model.GetValue(x) != null && manifest.Contains(x))
                .ToList();

            foreach (string name in properties)
            {
                if (model.GetValue(name) == null)
                {
                    continue;
                }

                ret.Add($"and {name} like '%' + @{name} + '%'");
            }

            return string.Join(" ", ret) + " ";
        }

        /// <summary>
        /// 資料轉換產生SQL insert 插入欄位及插入變數
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="model"></param>
        /// <returns>(<see cref="string"/> []) {"Property1, ...", "@Property1, ..." } </returns>
        protected string[] PropertiesToInsertCommands<T>(T model)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();
            List<string> propertyList = model.GetAllPropertyName();
            foreach (string item in propertyList)
            {
                if (model.GetValue(item) == null)
                {
                    continue;
                }

                ret.Add(item, $"@{item}");
            }

            return this.ToSQLCommands(ret);
        }

        /// <summary>
        /// 資料字典轉Sql
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        private string[] ToSQLCommands(Dictionary<string, string> dict)
            => new string[] { string.Join(",", new List<string>(dict.Keys)), string.Join(",", new List<string>(dict.Values)) };
    }
}
