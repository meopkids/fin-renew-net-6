﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 地段別查詢
    /// </summary>
    public class Jrcc0104_query
    {
        /// <summary>
        /// 縣市Id
        /// </summary>
        public string? City_id { get; set; }

        /// <summary>
        /// 鄉鎮Id
        /// </summary>
        public string? Town_id { get; set; }

        /// <summary>
        /// 地段Id
        /// </summary>
        public string? Sec_id { get; set; }

        /// <summary>
        /// 段名
        /// </summary>
        public string? Sec_name { get; set; }

        /// <summary>
        /// 小段
        /// </summary>
        public string? Sec_seq { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        public string? Note { get; set; }

        /// <summary>
        /// 事務所代碼
        /// </summary>
        public string? Llot_id { get; set; }
    }
}
