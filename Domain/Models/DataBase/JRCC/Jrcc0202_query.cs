﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 工作站查詢
    /// </summary>
    public class Jrcc0202_query
    {
        /// <summary>
        /// 工作站Id
        /// </summary>
        public string? Unit_id { get; set; }

        /// <summary>
        /// 區處Id
        /// </summary>
        public string? Division_id { get; set; }

        /// <summary>
        /// 工作站名
        /// </summary>
        public string? Unit_name { get; set; }

        /// <summary>
        /// 類別
        /// </summary>
        public string? C_type { get; set; }
    }
}
