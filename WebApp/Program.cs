using WebApp;

// Initialize builder and configure services
WebApplicationBuilder builder = WebApplication.CreateBuilder(args).ConfigureServices();

// Initialize App instance and configure middleware
WebApplication app = builder.Build().ConfigureMiddleWare();

app.Run();
