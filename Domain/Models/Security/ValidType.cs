﻿namespace Domain.Models.Security
{
    /// <summary>
    /// 驗證狀態
    /// </summary>
    public enum ValidType : byte
    {
        /// <summary>
        /// 驗證成功
        /// </summary>
        Success = 1,

        /// <summary>
        /// 驗證失敗
        /// </summary>
        Invalid = 2,

        /// <summary>
        /// Token過期
        /// </summary>
        Expired = 3,
    }
}
