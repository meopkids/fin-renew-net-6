﻿namespace Domain.Models.Security
{
    /// <summary>
    /// Auth Token
    /// </summary>
    public class Token
    {
        /// <summary>
        /// Token
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Refresh Token
        /// </summary>
        public string RefreshToken { get; set; }
    }
}
