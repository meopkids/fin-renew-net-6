﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 鄉鎮別
    /// </summary>
    public class Jrcc0103_get
    {
        /// <summary>
        /// 縣市Id
        /// </summary>
        /// <example>H</example>
        public string? City_id { get; set; }

        /// <summary>
        /// 鄉鎮Id
        /// </summary>
        /// <example>01</example>
        public string? Town_id { get; set; }

        /// <summary>
        /// 鄉鎮名
        /// </summary>
        /// <example>桃園區</example>
        public string? Town_name { get; set; }

        /// <summary>
        /// 郵遞區號
        /// </summary>
        /// <example>330</example>
        public string? Post_code { get; set; }
    }
}
