﻿namespace Domain.Models.Security
{
    /// <summary>
    /// 使用者資訊
    /// </summary>
    public class User
    {
        /// <summary>
        /// uid
        /// </summary>
        public string Account { get; set; } = "";

        /// <summary>
        /// 使用者分類
        /// </summary>
        public string User_type { get; set; } = "";

        /// <summary>
        /// 管理處
        /// </summary>
        public string Division_id { get; set; } = "";

        /// <summary>
        /// 工作站ID
        /// </summary>
        public string Unit_id { get; set; } = "";
    }
}
