﻿namespace Domain.Models.DataBase.U042.S0010
{
    /// <summary>
    /// 占用資料管理列表
    /// </summary>
    public class U042S0010_list
    {
        /// <summary>
        /// land0420 primary key
        /// </summary>
        /// <example>8dbe40f2-ba77-4a73-8838-0037c17bacf3</example>
        public Guid Id { get; set; }

        /// <summary>
        /// 檔案編號
        /// </summary>
        /// <example>104120008</example>
        public string Files_no { get; set; }

        /// <summary>
        /// 縣市
        /// </summary>
        /// <example>F</example>
        public string City_id { get; set; }

        /// <summary>
        /// 鄉鎮
        /// </summary>
        /// <example>18</example>
        public string Town_id { get; set; }

        /// <summary>
        /// 地段
        /// </summary>
        /// <example>1859</example>
        public string Sec_id { get; set; }

        /// <summary>
        /// 母號
        /// </summary>
        /// <example>0328</example>
        public string Land_no1 { get; set; }

        /// <summary>
        /// 子號
        /// </summary>
        /// <example>0001</example>
        public string Land_no2 { get; set; }

        /// <summary>
        /// 地目
        /// </summary>
        /// <example>水</example>
        public string? Land_category_name { get; set; }

        /// <summary>
        /// 占用面積
        /// </summary>
        /// <example>10.95</example>
        public double? Land_area { get; set; }

        /// <summary>
        /// 管理單位
        /// </summary>
        /// <example>99</example>
        public string? Division_id { get; set; }

        /// <summary>
        /// 工作站
        /// </summary>
        /// <example>12</example>
        public string? Unit_id { get; set; }

        /// <summary>
        /// 管理區處
        /// </summary>
        /// <example>工作站-海山</example>
        public string? Work_place { get; set; }

        /// <summary>
        /// 管理員簽核
        /// </summary>
        /// <example>*</example>
        public string Admin_checked { get; set; }

        /// <summary>
        /// 站長簽核
        /// </summary>
        /// <example>*</example>
        public string Unitmanager_checked { get; set; }

        /// <summary>
        /// 本處簽核
        /// </summary>
        /// <example>*</example>
        public string Office_checked { get; set; }

        /// <summary>
        /// 占用狀態
        /// </summary>
        /// <example>*</example>
        public string Occupied_status { get; set; }
    }
}
