﻿namespace Domain.Models.DataBase.U042.S0010
{
    /// <summary>
    /// 占用資料主檔
    /// </summary>
    public class Land0420_get
    {
        /// <summary>
        /// land0420 primary key
        /// </summary>
        /// <example>8dbe40f2-ba77-4a73-8838-0037c17bacf3</example>
        public Guid Id { get; set; }

        /// <summary>
        /// 檔案編號
        /// </summary>
        /// <example>104120008</example>
        public string Files_no { get; set; }

        /// <summary>
        /// Files_no_old
        /// </summary>
        /// <example>1718</example>
        public string? Files_no_old { get; set; }

        /// <summary>
        /// 土地類型
        /// </summary>
        /// <example>L</example>
        public string? Types { get; set; }

        /// <summary>
        /// 池塘編號
        /// </summary>
        public string? Pond_no { get; set; }

        /// <summary>
        /// 占用類型
        /// </summary>
        /// <example>240</example>
        public string? Occupy_type { get; set; }

        /// <summary>
        /// 縣市
        /// </summary>
        /// <example>F</example>
        public string City_id { get; set; }

        /// <summary>
        /// 鄉鎮
        /// </summary>
        /// <example>18</example>
        public string Town_id { get; set; }

        /// <summary>
        /// 地段
        /// </summary>
        /// <example>1859</example>
        public string Sec_id { get; set; }

        /// <summary>
        /// 母號
        /// </summary>
        /// <example>0328</example>
        public string Land_no1 { get; set; }

        /// <summary>
        /// 子號
        /// </summary>
        /// <example>0001</example>
        public string Land_no2 { get; set; }

        /// <summary>
        /// 地目代碼
        /// </summary>
        /// <example>E</example>
        public string? Land_category_id { get; set; }

        /// <summary>
        /// 等則
        /// </summary>
        public string? Land_kind { get; set; }

        /// <summary>
        /// 占用面積
        /// </summary>
        /// <example>10.95</example>
        public double Land_area { get; set; }

        /// <summary>
        /// 權利範圍類別
        /// </summary>
        /// <example>A</example>
        public string? P_bound_id { get; set; }

        /// <summary>
        /// 使用分區代碼
        /// </summary>
        /// <example>BA</example>
        public string? Land_use_id { get; set; }

        /// <summary>
        /// 使用地類別代碼
        /// </summary>
        public string? Land_classification_id { get; set; }

        /// <summary>
        /// 管理區處
        /// </summary>
        /// <example>99</example>
        public string Division_id { get; set; }

        /// <summary>
        /// 工作站
        /// </summary>
        /// <example>12</example>
        public string Unit_id { get; set; }

        /// <summary>
        /// 異動人員
        /// </summary>
        /// <example>t103</example>
        public string Update_id { get; set; }

        /// <summary>
        /// 異動日期
        /// </summary>
        /// <example>1060309</example>
        public string Update_date { get; set; }
    }
}
