﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 縣市別
    /// </summary>
    public class Jrcc0102_get
    {
        /// <summary>
        /// 縣市Id
        /// </summary>
        /// <example>H</example>
        public string City_id { get; set; }

        /// <summary>
        /// 縣市名
        /// </summary>
        /// <example>桃園巿</example>
        public string City_name { get; set; }
    }
}
