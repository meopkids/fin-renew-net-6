﻿using DataAccess.Models;

namespace DataAccess.Interfaces
{
    /// <summary>
    /// DapperService Interface
    /// </summary>
    public interface IDapperService
    {
        /// <summary>
        /// 執行命令
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Execute(DapperConnectModel model);

        /// <summary>
        /// 執行命令(非同步)
        /// </summary>
        /// <param name="model"></param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task<int> ExecuteAsync(DapperConnectModel model);

        /// <summary>
        /// 全部回傳
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        List<T> Query<T>(DapperConnectModel model);

        /// <summary>
        /// 全部回傳(非同步)
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<List<T>> QueryAsync<T>(DapperConnectModel model);

        /// <summary>
        /// 會將符合條件的第一筆回傳回來，如果沒有符合回傳null。
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        T QueryFirstOrDefault<T>(DapperConnectModel model);

        /// <summary>
        /// 會將符合條件的第一筆回傳回來，如果沒有符合回傳null。(非同步)
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<T> QueryFirstOrDefaultAsync<T>(DapperConnectModel model);

        /// <summary>
        /// 查詢唯一符合條件的資料，如果沒有符合或符合條件為多筆時會拋出錯誤。
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        T QuerySingle<T>(DapperConnectModel model);

        /// <summary>
        /// 查詢唯一符合條件的資料，如果沒有符合或符合條件為多筆時會拋出錯誤。(非同步)
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<T> QuerySingleAsync<T>(DapperConnectModel model);

        /// <summary>
        /// 查詢唯一符合條件的資料，如果沒有符合回傳null，但如果符合條件為多筆時會拋出錯誤。
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        T QuerySingleOrDefault<T>(DapperConnectModel model);

        /// <summary>
        /// 查詢唯一符合條件的資料，如果沒有符合回傳null，但如果符合條件為多筆時會拋出錯誤。(非同步)
        /// </summary>
        /// <typeparam name="T">傳出模型</typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<T> QuerySingleOrDefaultAsync<T>(DapperConnectModel model);
    }
}
