﻿using BusinessLogic.Interfaces;
using Domain.Models.General;
using Microsoft.AspNetCore.Mvc;

namespace BusinessLogic.Services
{
    /// <summary>
    /// HTTP狀態碼管理服務
    /// 依據 <see cref="GeneralResponse"/>.Status 狀態統一回復資料
    /// </summary>
    public class HTTPStatusWrapService : ControllerBase, IHTTPStatusWrap
    {
        /// <summary>
        /// UniResponse
        /// <list type="Response">
        /// <item> 200 Ok:
        /// <description>Sync Method Success </description>
        /// </item>
        /// <item> 202 Accepted:
        /// <description>Async Method</description>
        /// </item>
        /// </list>
        /// <see cref="https://docs.microsoft.com/zh-tw/azure/architecture/best-practices/api-design#delete-methods"/>
        /// </summary>
        /// <param name="general"></param>
        /// <returns></returns>
        public IActionResult UniResponse(GeneralResponse general)
        {
            // 需要細分時再客製
            switch (general.Status)
            {
                case int code when code >= 200 && code < 300:
                    return this.Ok(general.Row);
                case int code when code >= 400 && code < 500:
                    return this.BadRequest(general.Row);
                default:
                    return this.StatusCode(general.Status);
            }
        }
    }
}
