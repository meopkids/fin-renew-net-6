﻿namespace UtilityLib.Interfaces.Encryptions
{
    /// <summary>
    /// AES加密
    /// </summary>
    public interface IAesEncryptService
    {
        /// <summary>
        /// 加密字串
        /// </summary>
        /// <param name="plainText">明文</param>
        /// <param name="key">鑰匙</param>
        /// <param name="iv">IV鑰匙</param>
        /// <returns>已加密的字串</returns>
        string EncryptData(string plainText, string key, string iv);

        /// <summary>
        /// 解密字串
        /// </summary>
        /// <param name="plainText">密文</param>
        /// <param name="key">鑰匙</param>
        /// <param name="iv">IV鑰匙</param>
        /// <returns>已解密的字串</returns>
        string DecryptData(string plainText, string key, string iv);
    }
}
