﻿namespace UtilityLib.Extensions
{
    /// <summary>
    /// 基於民國曆擴充日期方法
    /// </summary>
    public static class TaiwanCalendarExtension
    {
        /// <summary>
        ///  回傳指定日期 格式日期: YYYmmdd (民國年)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="deltaDays"></param>
        /// <returns></returns>
        public static string GetROCFormatDate(this DateTime dt, int deltaDays = 0)
        {
            System.Globalization.TaiwanCalendar tc = new System.Globalization.TaiwanCalendar();
            dt = dt.AddDays(deltaDays);
            return string.Format("{0:000}{1:00}{2:00}", tc.GetYear(dt), tc.GetMonth(dt), tc.GetDayOfMonth(dt));
        }

        /// <summary>
        ///  回傳指定日期 格式日期: YYYmmdd (民國年)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="deltaDays"></param>
        /// <returns></returns>
        public static string GetROCFormatDateTime(this DateTime dt, int deltaDays = 0)
        {
            System.Globalization.TaiwanCalendar tc = new System.Globalization.TaiwanCalendar();
            dt = dt.AddDays(deltaDays);
            return string.Format("{0:000}{1:00}{2:00}{3:00}{4:00}{5:00}", tc.GetYear(dt), tc.GetMonth(dt), tc.GetDayOfMonth(dt), tc.GetHour(dt), tc.GetMinute(dt), tc.GetSecond(dt));
        }

        /// <summary>
        /// 回傳指定日期 格式日期: YYY (民國年)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="deltaDays"></param>
        /// <returns></returns>
        public static string GetROCFYear(this DateTime dt, int deltaDays = 0)
        {
            System.Globalization.TaiwanCalendar tc = new System.Globalization.TaiwanCalendar();
            dt = dt.AddDays(deltaDays);
            return string.Format("{0:000}", tc.GetYear(dt));
        }
    }
}
