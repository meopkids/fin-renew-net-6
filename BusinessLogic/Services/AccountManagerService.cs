﻿using BusinessLogic.Interfaces;
using BusinessLogic.Services.Base;
using DataAccess.Interfaces;
using Domain.Models.Account;
using Domain.Models.Security;

namespace BusinessLogic.Services
{
    /// <summary>
    /// 帳號、登入管理服務
    /// </summary>
    public class AccountManagerService : BaseOrdinaryService
    {
        #region 引入服務

        /// <summary>
        /// Token 管理服務
        /// </summary>
        private readonly ITokenManagerService _token;

        #endregion

        #region 注入/初始化

        /// <summary>
        /// 注入/初始化
        /// </summary>
        /// <param name="dapperRepository"></param>
        /// <param name="tokenManager"></param>
        public AccountManagerService(IDapperRepository dapperRepository, ITokenManagerService tokenManager)
        {
            this.DapperRepository = dapperRepository;
            this._token = tokenManager;
        }

        #endregion

        #region 公開方法

        /// <summary>
        /// 身分識別
        /// </summary>
        /// <param name="sqlParams">登入資訊</param>
        /// <returns></returns>
        public User? Identify(LoginModel sqlParams)
        {
            if (string.IsNullOrEmpty(sqlParams.Account) || string.IsNullOrEmpty(sqlParams.Password))
            {
                return null;
            }

            string whereClause = " WHERE user_id = @Account and password = @Password ";
            try
            {
                return this.DapperRepository.Authenticate(whereClause, sqlParams);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 取得Token
        /// </summary>
        /// <param name="userinfo">使用者資訊</param>
        /// <returns>(<see cref="Token"/>) 安全令牌</returns>
        public Token GetToken(User userinfo)
        {
            Token ret = this._token.Create(userinfo);
            this.StoreRefreshToken(userinfo, ret.RefreshToken);

            return ret;
        }

        #endregion

        #region 非公開方法

        /// <summary>
        /// 回存Refresh Token
        /// </summary>
        /// <param name="userinfo"></param>
        /// <param name="refreshToken"></param>
        private void StoreRefreshToken(User userinfo, string refreshToken)
        {
            string sqlCmd = @$"
                UPDATE asys0101 
                SET token = @RefreshToken
                WHERE user_id = @Account";

            this.DapperRepository.StoreRefreshToken(sqlCmd, new { userinfo.Account, RefreshToken = refreshToken });
        }

        #endregion
    }
}
