﻿using BusinessLogic.Interfaces;
using Domain.Models.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using UtilityLib.Extensions;

namespace BusinessLogic.Attributes
{
    /// <summary>
    /// 以Token檢查身份
    /// </summary>
    public class CustomAuthorizeAttribute : IAuthorizationFilter
    {
        /// <summary>
        /// 取得環境變數
        /// </summary>
        private readonly IWebHostEnvironment _hostingEnv;

        /// <summary>
        /// 取得HttpContextAccessor
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// 安全令牌管理服務
        /// </summary>
        private readonly ITokenManagerService _tokenManager;

        /// <summary>
        /// Session
        /// </summary>
        private ISession Session => this._httpContextAccessor.HttpContext !.Session;

        /// <summary>
        /// 非產品環境
        /// </summary>
        private bool IsNotProduction => this._hostingEnv.EnvironmentName != "Production";

        /// <summary>
        /// 注入/初始化
        /// </summary>
        /// <param name="webHost"></param>
        /// <param name="httpContext"></param>
        /// <param name="tokenManager"></param>
        public CustomAuthorizeAttribute(IWebHostEnvironment webHost, IHttpContextAccessor httpContext, ITokenManagerService tokenManager)
        {
            this._hostingEnv = webHost;
            this._httpContextAccessor = httpContext;
            this._tokenManager = tokenManager;
        }

        /// <summary>
        /// 檢查安全令牌(Token)
        /// </summary>
        /// <param name="context"></param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            // ValidationContext.GetService();
            // 是否從Swaager UI 發出請求
            bool isSwagger = !string.IsNullOrEmpty(context.HttpContext.Request.Headers["swagger-ui"]);

            string inputToken = context.HttpContext.Request.Headers["Authorization"];

            // 非Production 環境上 從Swagger UI發的Request 如果沒有token，自動帶入
            string token = (isSwagger && string.IsNullOrEmpty(inputToken) && this.IsNotProduction) ?
                "06b90bd55c37452f.sCSciQNZpqKz1Dy/Y3g4KdRMp7k6cY2LZui448xfJK7FQ1g+6utqYl09VSmd7VAgR3Ig8O404o07gha2irsTaZ26IZT41AHUgkqWO240Epw4vhCTGyZ7xtukd3ZtQTqVOEDqZh2rEEck0Gir2vkOKdvq5T0m2K0PlozyUGCPj2W0Giy7OE9aT4BmWtg1Fuv2sCucjlyL9yodk0w7AxxJORpHvILxEVj2/6/+S2l7ECZdExq/sbSejaNUazYpn7NKiEzu48hLQZtLDFO5rl+I5g==.8EB5AFFE9639D57101CE88597C00640F40B2A9F8DE2F9F6F1CE96BA77D6F9188"
                : inputToken;

            if (string.IsNullOrEmpty(token))
            {
                context.Result = new ContentResult { StatusCode = 401, ContentType = "application/json", Content = new { Status = "請登入取得安全令牌", ValidCode = ValidType.Invalid }.ToJson() };
                return;
            }

            // 驗證令牌
            AuthenticationResult result = this._tokenManager.Verify(token);

            // 讓前端分辨是否Expired
            switch (result.Status)
            {
                case ValidType.Invalid:
                    context.Result = new ContentResult { StatusCode = 401, ContentType = "application/json", Content = new { Status = "驗證失敗", ValidCode = result.Status }.ToJson() };
                    return;

                case ValidType.Expired:
                    context.Result = new ContentResult { StatusCode = 401, ContentType = "application/json", Content = new { Status = "驗證過期", ValidCode = result.Status }.ToJson() };
                    return;

                case ValidType.Success:
                    // 設定登入資訊
                    this.Session.SetObjectAsJson("user_context", result.UserInfo);
                    return;

                default:
                    break;
            }
        }
    }
}
