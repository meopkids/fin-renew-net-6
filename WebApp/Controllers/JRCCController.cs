﻿using BusinessLogic.Interfaces;
using BusinessLogic.Services;
using Domain.Models.DataBase.JRCC;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    /// <summary>
    /// jrcc參考路由
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class JRCCController : ControllerBase
    {
        #region 引入服務

        /// <summary>
        /// 狀態碼管理服務
        /// </summary>
        private readonly IHTTPStatusWrap _wrapper;

        /// <summary>
        /// 參考資料
        /// </summary>
        private readonly JRCCReferenceService _ref;

        #endregion

        #region 注入/初始化

        /// <summary>
        /// 注入/初始化
        /// </summary>
        /// <param name="httpWrapper"></param>
        /// <param name="jrccData"></param>
        public JRCCController(IHTTPStatusWrap httpWrapper, JRCCReferenceService jrccData)
        {
            this._wrapper = httpWrapper;
            this._ref = jrccData;
        }

        #endregion

        #region 公開方法

        /// <summary>
        /// 縣市別 jrcc0102
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("city")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0102_get>), 200)]
        public IActionResult GetCity([FromQuery] Jrcc0102_query query)
        {
            return this._wrapper.UniResponse(this._ref.CityReference(query));
        }

        /// <summary>
        /// 鄉鎮別 jrcc0103
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("town")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0103_get>), 200)]
        public IActionResult GetTown([FromQuery] Jrcc0103_query query)
        {
            return this._wrapper.UniResponse(this._ref.TownReference(query));
        }

        /// <summary>
        /// 地段別 jrcc0104
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("sec")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0104_get>), 200)]
        public IActionResult GetSec([FromQuery] Jrcc0104_query query)
        {
            return this._wrapper.UniResponse(this._ref.SecReference(query));
        }

        /// <summary>
        /// 地目別 jrcc0106
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("landcategory")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0106_get>), 200)]
        public IActionResult GetLandCategory([FromQuery] Jrcc0106_query query)
        {
            return this._wrapper.UniResponse(this._ref.LandCategoryReference(query));
        }

        /// <summary>
        /// 使用分區 jrcc0107
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("landuse")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0107_get>), 200)]
        public IActionResult GetLandUse([FromQuery] Jrcc0107_query query)
        {
            return this._wrapper.UniResponse(this._ref.LandUseReference(query));
        }

        /// <summary>
        /// 使用地類別 jrcc0108
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("landclassification")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0108_get>), 200)]
        public IActionResult GetLandClassification([FromQuery] Jrcc0108_query query)
        {
            return this._wrapper.UniResponse(this._ref.LandClassificationReference(query));
        }

        /// <summary>
        /// 區處 jrcc0201
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("division")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0201_get>), 200)]
        public IActionResult GetDivision([FromQuery] Jrcc0201_query query)
        {
            return this._wrapper.UniResponse(this._ref.DivisionReference(query));
        }

        /// <summary>
        /// 工作站 jrcc0202
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("unit")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0202_get>), 200)]
        public IActionResult GetUnitReference([FromQuery] Jrcc0202_query query)
        {
            return this._wrapper.UniResponse(this._ref.UnitReference(query));
        }

        /// <summary>
        /// 土地用途別 jrcc0206
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("use")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0206_get>), 200)]
        public IActionResult GetUse([FromQuery] Jrcc0206_query query)
        {
            return this._wrapper.UniResponse(this._ref.UseReference(query));
        }

        /// <summary>
        /// 等則 jrcc0301
        /// </summary>
        /// <param name="query">查詢資訊</param>
        /// <returns></returns>
        /// <response code="200">回傳資料</response>
        /// <response code="400">發生錯誤</response>
        [HttpGet]
        [Route("landkind")]
        [Produces(contentType: "application/json")]
        [ProducesResponseType(typeof(List<Jrcc0301_get>), 200)]
        public IActionResult GetLandKind([FromQuery] Jrcc0301_query query)
        {
            return this._wrapper.UniResponse(this._ref.LandKindReference(query));
        }
        #endregion
    }
}
