﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 使用地類別
    /// </summary>
    public class Jrcc0108_get
    {
        /// <summary>
        /// 使用地類別Id
        /// </summary>
        /// <example>EA</example>
        public string Land_classification_id { get; set; }

        /// <summary>
        /// 使用地類別名
        /// </summary>
        /// <example>甲種建築用地</example>
        public string Land_classification_name { get; set; }
    }
}
