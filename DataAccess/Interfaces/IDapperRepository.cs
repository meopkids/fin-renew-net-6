﻿using Domain.Models.Security;

namespace DataAccess.Interfaces
{
    /// <summary>
    /// DapperRepo interface
    /// </summary>
    public interface IDapperRepository
    {
        #region Account

        /// <summary>
        /// 取得使用者資料
        /// 如果沒有符合的資料返回null，符合多筆丟出錯誤
        /// </summary>
        /// <param name="whereClause"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public User Authenticate(string whereClause, object? sqlParams = null);

        /// <summary>
        /// 回存Refresh Token
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public int StoreRefreshToken(string sqlCmd, object? sqlParams = null);

        #endregion

        #region Security

        /// <summary>
        /// 取得APP的加密金鑰
        /// Key  Length: 76
        /// </summary>
        /// <returns></returns>
        public EncryptKey GetEncryptKey();

        #endregion

        #region 共用

        /// <summary>
        /// SQL 查詢
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="sqlCmd"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public List<T> Query<T>(string sqlCmd, object? sqlParams = null);

        /// <summary>
        /// SQL 執行
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public int Execute(string sqlCmd, object? sqlParams = null);

        /// <summary>
        /// 取得第一筆資料
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="sqlCmd"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public T QueryFirstOrDefault<T>(string sqlCmd, object? query = null);

        /// <summary>
        /// 取得單一筆資料，有多筆符合條件會拋錯
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="sqlCmd"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public T QuerySingleOrDefault<T>(string sqlCmd, object? query = null);

        /// <summary>
        /// 共用方法: 新增資料
        /// </summary>
        /// <param name="tableName">資料表名稱</param>
        /// <param name="fields">欄位</param>
        /// <param name="values">欄位值</param>
        /// <param name="sqlParams">變數值</param>
        /// <returns>The infected Rows(<see cref="int"/>).</returns>
        public int InsertDB(string tableName, string fields, string values, object sqlParams);

        /// <summary>
        /// 共用方法: 更新資料
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="updateCmds"></param>
        /// <param name="whereClause"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public int UpdateDB(string tableName, string updateCmds, string whereClause, object sqlParams);

        #endregion
    }
}
