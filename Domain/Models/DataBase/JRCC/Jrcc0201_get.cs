﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 區處
    /// </summary>
    public class Jrcc0201_get
    {
        /// <summary>
        /// 區處Id
        /// </summary>
        /// <example>99</example>
        public string Division_id { get; set; }

        /// <summary>
        /// 區處名
        /// </summary>
        /// <example>工作站</example>
        public string Division_name { get; set; }
    }
}
