﻿using System.Text.Json.Serialization;

namespace Domain.Models.DataBase.Enum
{
    /// <summary>
    /// 占用期間
    /// </summary>
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum OcupiedPeriodEnum
    {
        /// <summary>
        /// 已到期
        /// </summary>
        Y,

        /// <summary>
        /// 未到期
        /// </summary>
        N,
    }
}
