﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 使用分區查詢
    /// </summary>
    public class Jrcc0107_query
    {
        /// <summary>
        /// 使用分區Id
        /// </summary>
        public string? Land_use_id { get; set; }

        /// <summary>
        /// 使用分區名
        /// </summary>
        public string? Land_use_name { get; set; }
    }
}
