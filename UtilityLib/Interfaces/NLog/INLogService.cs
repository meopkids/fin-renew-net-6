﻿namespace UtilityLib.Interfaces.NLog
{
    /// <summary>
    /// NLog
    /// </summary>
    public interface INLogService
    {
        /// <summary>
        /// 訊息，記錄不影響系統執行的訊息，通常會記錄登入登出或是資料的建立刪除、傳輸等。
        /// </summary>
        /// <param name="message"></param>
        void InfoLog(string message);

        /// <summary>
        /// 追蹤，紀錄操作歷程等。
        /// </summary>
        /// <param name="message"></param>
        void TraceLog(string message);

        /// <summary>
        /// 警告，用於需要提示的訊息，例如庫存不足、貨物超賣、餘額即將不足等。
        /// </summary>
        /// <param name="message"></param>
        void WarnLog(string message);

        /// <summary>
        /// 用於開發，於開發時將一些需要特別關注的訊息以Debug傳出。
        /// </summary>
        /// <param name="message"></param>
        void DebugLog(string message);

        /// <summary>
        /// 錯誤，記錄系統實行所發生的錯誤，例如資料庫錯誤、遠端連線錯誤、發生例外等。
        /// </summary>
        /// <param name="message"></param>
        void ErrorLog(string message);

        /// <summary>
        /// 錯誤，記錄系統實行所發生的錯誤，例如資料庫錯誤、遠端連線錯誤、發生例外等。
        /// </summary>
        /// <param name="message"></param>
        void FatalLog(string message);

        /// <summary>
        /// 訊息，記錄不影響系統執行的訊息，通常會記錄登入登出或是資料的建立刪除、傳輸等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        void InfoLog(string snamespace, Exception exception);

        /// <summary>
        /// 追蹤，紀錄操作歷程等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        void TraceLog(string snamespace, Exception exception);

        /// <summary>
        /// 警告，用於需要提示的訊息，例如庫存不足、貨物超賣、餘額即將不足等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        void WarnLog(string snamespace, Exception exception);

        /// <summary>
        /// 用於開發，於開發時將一些需要特別關注的訊息以Debug傳出。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        void DebugLog(string snamespace, Exception exception);

        /// <summary>
        /// 錯誤，記錄系統實行所發生的錯誤，例如資料庫錯誤、遠端連線錯誤、發生例外等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        void ErrorLog(string snamespace, Exception exception);

        /// <summary>
        /// 錯誤，記錄系統實行所發生的錯誤，例如資料庫錯誤、遠端連線錯誤、發生例外等。
        /// </summary>
        /// <param name="snamespace"></param>
        /// <param name="exception"></param>
        void FatalLog(string snamespace, Exception exception);
    }
}
