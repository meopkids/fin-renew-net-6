﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models.General
{
    /// <summary>
    /// 取得ID
    /// </summary>
    public class MyIdentity
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required]
        public Guid Id { get; set; }
    }
}
