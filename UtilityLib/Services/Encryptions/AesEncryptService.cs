﻿using System.Security.Cryptography;
using System.Text;
using UtilityLib.Interfaces.Encryptions;

namespace UtilityLib.Services.Encryptions
{
    /// <summary>
    /// AES加密
    /// </summary>
    public class AesEncryptService : IAesEncryptService
    {
        /// <summary>
        /// 加密字串
        /// </summary>
        /// <param name="plainText">明文</param>
        /// <param name="key">鑰匙</param>
        /// <param name="iv">IV鑰匙</param>
        /// <returns>已加密的字串</returns>
        public string EncryptData(string plainText, string key, string iv)
        {
            byte[] sourceBytes = Encoding.UTF8.GetBytes(plainText);
            Aes aes = Aes.Create();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            ICryptoTransform transform = aes.CreateEncryptor();

            return Convert.ToBase64String(transform.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length));
        }

        /// <summary>
        /// 解密字串
        /// </summary>
        /// <param name="plainText">密文</param>
        /// <param name="key">鑰匙</param>
        /// <param name="iv">IV鑰匙</param>
        /// <returns>已解密的字串</returns>
        public string DecryptData(string plainText, string key, string iv)
        {
            byte[] encryptBytes = System.Convert.FromBase64String(plainText);
            Aes aes = Aes.Create();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            ICryptoTransform transform = aes.CreateDecryptor();

            return Encoding.UTF8.GetString(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length));
        }
    }
}
