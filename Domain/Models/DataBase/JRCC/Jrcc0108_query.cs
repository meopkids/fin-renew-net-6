﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 使用地類別查詢
    /// </summary>
    public class Jrcc0108_query
    {
        /// <summary>
        /// 使用地類別Id
        /// </summary>
        public string? Land_classification_id { get; set; }

        /// <summary>
        /// 使用地類別名
        /// </summary>
        public string? Land_classification_name { get; set; }
    }
}
