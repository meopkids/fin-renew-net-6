﻿using BusinessLogic.Services.Base;
using DataAccess.Interfaces;
using Domain.Models.DataBase.JRCC;
using Domain.Models.General;
using Microsoft.AspNetCore.Http;
using System.Reflection;
using UtilityLib.Extensions;

namespace BusinessLogic.Services
{
    /// <summary>
    /// 參考資料 (jrcc)
    /// </summary>
    public class JRCCReferenceService : BaseExtraordinaryService
    {
        #region 引入其他服務
        #endregion

        #region 注入/初始化

        /// <summary>
        /// 注入/初始化
        /// </summary>
        /// <param name="dapperRepository"></param>
        /// <param name="httpContext"></param>
        public JRCCReferenceService(IDapperRepository dapperRepository, IHttpContextAccessor httpContext)
        {
            this.HttpContextAccessor = httpContext;
            this.DapperRepository = dapperRepository;
        }

        #endregion

        /// <summary>
        /// 縣市別 jrcc0102
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse CityReference(Jrcc0102_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            query.City_name = query.City_name.Bool() ? $"%{query.City_name}%" : query.City_name;

            whereClause += query.City_id.Bool() ? " AND city_id = @City_id" : "";
            whereClause += query.City_name.Bool() ? " AND city_name like @City_name" : "";

            string sqlCmd = $"SELECT * FROM jrcc0102 WHERE 1=1 {whereClause}";

            List<Jrcc0102_get> results = this.DapperRepository.Query<Jrcc0102_get>(sqlCmd, query);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 鄉鎮別 jrcc0103
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse TownReference(Jrcc0103_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            whereClause += query.City_id.Bool() ? " AND city_id = @City_id" : "";
            whereClause += query.Town_id.Bool() ? " AND town_id = @Town_id" : "";

            string sqlCmd = $"SELECT * FROM jrcc0103 WHERE 1=1 {whereClause}";

            List<Jrcc0103_get> results = this.DapperRepository.Query<Jrcc0103_get>(sqlCmd, query);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 地段別 jrcc0104
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse SecReference(Jrcc0104_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            whereClause += query.City_id.Bool() ? " AND city_id = @City_id" : "";
            whereClause += query.Town_id.Bool() ? " AND town_id = @Town_id" : "";
            whereClause += query.Sec_id.Bool() ? " AND sec_id = @Sec_id" : "";

            string sqlCmd = $"SELECT * FROM jrcc0104 WHERE 1=1 {whereClause}";

            List<Jrcc0104_get> results = this.DapperRepository.Query<Jrcc0104_get>(this.RowLIMTD(sqlCmd), query);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 地目別 jrcc0106
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse LandCategoryReference(Jrcc0106_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            whereClause += this.AttributeToWhereClause(query, new string[] { "Land_category_id", "Land_category_name" });

            string sqlCmd = $"SELECT * FROM jrcc0106 WHERE 1=1 {whereClause}";

            List<Jrcc0106_get> results = this.DapperRepository.Query<Jrcc0106_get>(sqlCmd, query);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 使用分區 jrcc0107
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse LandUseReference(Jrcc0107_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            whereClause += this.AttributeToWhereClause(query, new string[] { "Land_use_id", "Land_use_name" });

            string sqlCmd = $"SELECT * FROM jrcc0107 WHERE 1=1 {whereClause}";

            List<Jrcc0107_get> results = this.DapperRepository.Query<Jrcc0107_get>(sqlCmd, query);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 使用地類別 jrcc0108
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse LandClassificationReference(Jrcc0108_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            whereClause += this.AttributeToWhereClause(query, new string[] { "Land_classification_id", "Land_classification_name" });

            string sqlCmd = $"SELECT * FROM jrcc0108 WHERE 1=1 {whereClause}";

            List<Jrcc0108_get> results = this.DapperRepository.Query<Jrcc0108_get>(sqlCmd, query);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 區處 jrcc0201
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse DivisionReference(Jrcc0201_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            whereClause += this.AttributeToWhereClause(query, new string[] { "Division_id", "Division_name" });

            string sqlCmd = $"SELECT * FROM jrcc0201 WHERE 1=1 {whereClause}";

            List<Jrcc0201_get> results = this.DapperRepository.Query<Jrcc0201_get>(sqlCmd, query);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 工作站 jrcc0202
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse UnitReference(Jrcc0202_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            whereClause += this.AttributeToWhereClause(query, new string[] { "Unit_id", "Division_id", "Unit_name", "C_type" });

            string sqlCmd = $"SELECT * FROM jrcc0202 WHERE 1=1 {whereClause}";

            List<Jrcc0202_get> results = this.DapperRepository.Query<Jrcc0202_get>(sqlCmd, query);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 土地用途別 jrcc0206
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse UseReference(Jrcc0206_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            whereClause += this.AttributeToWhereClause(query, new string[] { "Use_id", "Use_name", "B_enterprise" });

            string sqlCmd = $"SELECT * FROM jrcc0206 WHERE 1=1 {whereClause}";

           // TODO: Better way to convert Enum Type to String
            Jrcc0206_get newQuery = query.AssignData<Jrcc0206_get>(new Jrcc0206_get());
            // object newQuery = new { query.Use_id, query.Use_name, B_enterprise = query.B_enterprise.ToString() };

            List<Jrcc0206_get> results = this.DapperRepository.Query<Jrcc0206_get>(sqlCmd, newQuery);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 等則別 jrcc0301
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse LandKindReference(Jrcc0301_query query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string whereClause = "";

            whereClause += query.Land_kind.Bool() ? " and land_kind = @Land_kind " : "";

            string sqlCmd = $"SELECT * FROM jrcc0301 WHERE 1=1 {whereClause}";

            List<Jrcc0301_get> results = this.DapperRepository.Query<Jrcc0301_get>(sqlCmd, query);

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }
    }
}
