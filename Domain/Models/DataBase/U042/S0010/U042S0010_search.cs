﻿using Domain.Models.DataBase.Enum;

namespace Domain.Models.DataBase.U042.S0010
{
    /// <summary>
    /// 占用列表查詢
    /// </summary>
    public class U042S0010_search
    {
        /// <summary>
        /// 占用期間 land0421.occupy_period_end
        /// Y: 已到期, N: 未到期
        /// </summary>
        public OcupiedPeriodEnum? Ocupied_period { get; set; }

        /// <summary>
        /// 土地類別 land0420
        /// B: 事業用地, N: 非事業用地
        /// </summary>
        public EnterpriseEnum? Enterprise { get; set; }

        /// <summary>
        /// 繳納狀態 land0422.pay_date、clear_date、pay_date_end
        /// Y: 已繳, N: 未繳, O: 逾期
        /// </summary>
        public PayStatusEnum? Pay_status { get; set; }

        /// <summary>
        /// 縣市
        /// </summary>
        public string? City_id { get; set; }

        /// <summary>
        /// 鄉鎮
        /// </summary>
        public string? Town_id { get; set; }

        /// <summary>
        /// 地段
        /// </summary>
        public string? Sec_id { get; set; }

        /// <summary>
        /// 母號起始
        /// </summary>
        public string? Land_no1_start { get; set; }

        /// <summary>
        /// 母號迄止
        /// </summary>
        public string? Land_no1_end { get; set; }

        /// <summary>
        /// 子號起始
        /// </summary>
        public string? Land_no2_start { get; set; }

        /// <summary>
        /// 子號迄止
        /// </summary>
        public string? Land_no2_end { get; set; }

        /// <summary>
        /// 區處
        /// </summary>
        public string? Division_id { get; set; }

        /// <summary>
        /// 工作站
        /// </summary>
        public string? Unit_id { get; set; }

        /// <summary>
        /// 結案結果
        /// </summary>
        public string? Close_status { get; set; }

        /// <summary>
        /// 占用人
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// 身分證號
        /// </summary>
        public string? Idcard_no { get; set; }

        /// <summary>
        /// 檔案編號
        /// </summary>
        public string? Files_no { get; set; }

        /// <summary>
        /// 虛擬編號
        /// </summary>
        public string? Fictitious_id { get; set; }

        /// <summary>
        /// 入帳日起始
        /// </summary>
        public string? Pay_date_start { get; set; }

        /// <summary>
        /// 入帳日迄止
        /// </summary>
        public string? Pay_date_end { get; set; }

        /// <summary>
        /// 繳費金額
        /// </summary>
        public decimal? Use_money { get; set; }
    }
}
