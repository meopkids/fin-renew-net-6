﻿namespace Domain.Caches
{
    /// <summary>
    /// SQLconnet
    /// </summary>
    public class ConnectionStrings
    {
        /// <summary>
        /// AppSettings.ConnectionStrings.SQLConnect
        /// </summary>
        public string SQLConnect { get; set; }
    }
}
