﻿using System.Text.Json.Serialization;

namespace Domain.Models.DataBase.Enum
{
    /// <summary>
    /// 是否為事業用地
    /// ToDisplay Enum Type as String in Swagger UI: https://stackoverflow.com/questions/36452468/swagger-ui-web-api-documentation-present-enums-as-strings
    /// </summary>
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum EnterpriseEnum
    {
        /// <summary>
        /// 是
        /// </summary>
        B,

        /// <summary>
        /// 否
        /// </summary>
        N,
    }
}
