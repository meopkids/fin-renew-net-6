﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 地目別
    /// </summary>
    public class Jrcc0106_get
    {
        /// <summary>
        /// 地目Id
        /// </summary>
        /// <example>0</example>
        public string Land_category_id { get; set; }

        /// <summary>
        /// 地目名
        /// </summary>
        /// <example>未</example>
        public string Land_category_name { get; set; }
    }
}
