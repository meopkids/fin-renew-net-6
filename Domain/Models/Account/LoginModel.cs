﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models.Account
{
    /// <summary>
    /// 登入
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        public string Account { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
