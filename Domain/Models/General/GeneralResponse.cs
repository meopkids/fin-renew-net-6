﻿namespace Domain.Models.General
{
    /// <summary>
    /// 統一回覆
    /// </summary>
    public class GeneralResponse
    {
        /// <summary>
        /// HTTP Code
        /// </summary>
        /// <example>200</example>
        public int Status { get; set; }

        /// <summary>
        /// 資料
        /// </summary>
        /// <example>新增成功</example>
        public object? Row { get; set; }
    }
}
