﻿using Domain.Models.DataBase.U042.S0010;
using FluentValidation;

namespace BusinessLogic.Validations
{
    /// <summary>
    /// Test
    /// </summary>
    public class TestValidation : AbstractValidator<Land0420_insert>
    {
        /// <summary>
        /// 初始化驗證器
        /// </summary>
        public TestValidation()
        {
            // Oid 驗證
            this.RuleFor(param => param.Files_no_old)
                .NotEmpty()
                .WithMessage($"{nameof(Land0420_insert.Files_no_old)}必填");
        }
    }
}
