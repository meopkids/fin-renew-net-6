﻿using Microsoft.AspNetCore.Http;
using UtilityLib.Extensions;
using UtilityLib.Interfaces.NLog;

namespace UtilityLib.Middlewares
{
    /// <summary>
    /// ExceptionMiddleware
    /// </summary>
    public class ExceptionMiddleware
    {
        /// <summary>
        /// RequestDelegate
        /// </summary>
        private readonly RequestDelegate _next;

        /// <summary>
        /// NLog
        /// </summary>
        private readonly INLogService _nLogService;

        /// <summary>
        /// init
        /// </summary>
        /// <param name="next"></param>
        /// <param name="nLogService"></param>
        public ExceptionMiddleware(RequestDelegate next, INLogService nLogService)
        {
            this._next = next;
            this._nLogService = nLogService;
        }

        /// <summary>
        /// request Invoke
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await this._next(context);
            }
            catch (Exception ex)
            {
                string route = context.Request.Path.Value !;
                string errorMessage = $"Catch Exception from Middleware : {route} -> {ex.Message}";
                this._nLogService.ErrorLog(errorMessage, ex);
                await HandleExceptionRyuukiAsync(context, ex);
            }
        }

        /// <summary>
        /// 改良後方法
        /// </summary>
        /// <param name="context"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static Task HandleExceptionRyuukiAsync(HttpContext context, Exception ex)
        {
            object resualt = new { StateCode = 500, Detail = ex.Message };

            context.Response.Clear();
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = 500;
            return context.Response.WriteAsync(resualt.ToJson());
        }
    }
}
