﻿using System.Text.Json.Serialization;

namespace Domain.Models.DataBase.Enum
{
    /// <summary>
    /// 繳款狀態
    /// </summary>
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum PayStatusEnum
    {
        /// <summary>
        /// 已繳
        /// </summary>
        Y,

        /// <summary>
        /// 未繳
        /// </summary>
        N,

        /// <summary>
        /// 逾期
        /// </summary>
        O,
    }
}
