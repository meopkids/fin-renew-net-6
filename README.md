桃園水利處財務系統翻新專案資料說明
==

環境
```
1. netcore 6
2. sdk version 6.0.202
```


CLI
```
1. 環境檢查: dotnet --info
2. gitignore 範本: dotnet new gitignore
3. 版本控管: dotnet new globaljson --sdk-version <version>
4. 發布: dotnet publish -c Release -r <rid> --no-self-contained -o <target_folder> /p:EnvironmentName=Staging
```
注意事項
```
	
1. 執行SelectPdf
- 可能會遇到錯誤 Conversion failure. Could not find ‘Select.Html.dep’.
- 需要設定Select.Html.dep 屬性: (1) 建置: 內容 (2)複製到輸出目錄: 永遠複製
- Local端執行IIS可能無法使用，需要部屬
- How to fix: https://selectpdf.com/how-to-fix-conversion-failure-could-not-find-select-html-dep/
- Deployment: https://selectpdf.com/html-to-pdf/docs/html/Deployment.htm

```

[Document comment](https://docs.microsoft.com/zh-tw/dotnet/csharp/language-reference/language-specification/documentation-comments)
