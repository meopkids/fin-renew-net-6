﻿namespace DataAccess.Models
{
    /// <summary>
    /// Dapper資料庫連接模型
    /// </summary>
    public class DapperConnectModel
    {
        /// <summary>
        /// 資料庫連接字串
        /// </summary>
        public string SqlConnectionString { get; set; }

        /// <summary>
        /// 資料庫指令碼
        /// </summary>
        public string SqlCommand { get; set; }

        /// <summary>
        /// Execute 資料
        /// </summary>
        public object? SqlParamData { get; set; } = null;
    }
}
