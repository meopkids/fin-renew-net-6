﻿namespace Domain.Models.Security
{
    /// <summary>
    /// 加密金鑰
    /// </summary>
    public class EncryptKey
    {
        /// <summary>
        /// APP 金鑰
        /// </summary>
        public string App_encrypt_key { get; set; }
    }
}
