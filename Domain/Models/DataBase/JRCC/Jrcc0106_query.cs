﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 地目別查詢
    /// </summary>
    public class Jrcc0106_query
    {
        /// <summary>
        /// 地目Id
        /// </summary>
        public string? Land_category_id { get; set; }

        /// <summary>
        /// 地目名
        /// </summary>
        public string? Land_category_name { get; set; }
    }
}
