﻿using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    /// <summary>
    /// 繳款單
    /// </summary>
    public class PaymentController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return this.View("~/Pages/Payment/Index");
        }
    }
}
