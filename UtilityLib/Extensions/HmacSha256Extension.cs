﻿using System.Security.Cryptography;
using System.Text;

namespace UtilityLib.Extensions
{
    /// <summary>
    /// HMAC - SHA256 加密
    /// </summary>
    public static class HmacSha256Extension
    {
        /// <summary>
        /// HMAC-SHA256 加密
        /// </summary>
        /// <param name="message"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string HMACSHA256(this string message, string key)
        {
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            byte[] keyByte = Encoding.UTF8.GetBytes(key);
            HMACSHA256 hmacSHA256 = new HMACSHA256(keyByte);
            byte[] hashMessage = hmacSHA256.ComputeHash(messageBytes);

            return BitConverter.ToString(hashMessage).Replace("-", "").ToUpper();
        }
    }
}
