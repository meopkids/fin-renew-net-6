﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 等則別
    /// </summary>
    public class Jrcc0301_get
    {
        /// <summary>
        /// 等則Id
        /// </summary>
        /// <example>01</example>
        public string Land_kind { get; set; }

        /// <summary>
        /// 金額
        /// </summary>
        /// <example>3174</example>
        public double Lease_money { get; set; }
    }
}
