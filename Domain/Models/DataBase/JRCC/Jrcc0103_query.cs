﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 鄉鎮別查詢
    /// </summary>
    public class Jrcc0103_query
    {
        /// <summary>
        /// 縣市Id
        /// </summary>
        public string? City_id { get; set; }

        /// <summary>
        /// 鄉鎮Id
        /// </summary>
        public string? Town_id { get; set; }

        /// <summary>
        /// 鄉鎮名
        /// </summary>
        public string? Town_name { get; set; }

        /// <summary>
        /// 郵遞區號
        /// </summary>
        public string? Post_code { get; set; }
    }
}
