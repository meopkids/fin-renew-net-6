﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models.DataBase.U042.S0010
{
    /// <summary>
    /// land0420 異動資料
    /// </summary>
    public class Land0420_update
    {
        /// <summary>
        /// 檔案編號
        /// </summary>
        /// <example>104120008</example>
        [Required]
        public string Files_no { get; set; }

        /// <summary>
        /// 舊檔案編號
        /// </summary>
        /// <example>1718</example>
        public string? Files_no_old { get; set; }

        /// <summary>
        /// 縣市
        /// </summary>
        /// <example>F</example>
        [Required]
        public string City_id { get; set; }

        /// <summary>
        /// 鄉鎮
        /// </summary>
        /// <example>18</example>
        [Required]
        public string Town_id { get; set; }

        /// <summary>
        /// 地段
        /// </summary>
        /// <example>1859</example>
        [Required]
        public string Sec_id { get; set; }

        /// <summary>
        /// 母號
        /// </summary>
        /// <example>0328</example>
        [Required]
        public string Land_no1 { get; set; }

        /// <summary>
        /// 子號
        /// </summary>
        /// <example>0001</example>
        [Required]
        public string Land_no2 { get; set; }

        /// <summary>
        /// 區處
        /// </summary>
        /// <example>99</example>
        [Required]
        public string Division_id { get; set; }

        /// <summary>
        /// 工作站
        /// </summary>
        /// <example>12</example>
        [Required]
        public string Unit_id { get; set; }
    }
}
