﻿using Domain.Models.DataBase.Enum;

namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 用途別查詢
    /// </summary>
    public class Jrcc0206_query
    {
        /// <summary>
        /// 用途Id
        /// </summary>
        public string? Use_id { get; set; }

        /// <summary>
        /// 用途名
        /// </summary>
        public string? Use_name { get; set; }

        /// <summary>
        /// 事業/非事業
        /// B: 事業用地, N: 非事業用地
        /// </summary>
        public EnterpriseEnum? B_enterprise { get; set; }
    }
}
