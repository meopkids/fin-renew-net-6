﻿namespace Domain.Models.Security
{
    /// <summary>
    /// Payload
    /// </summary>
    public class Payload
    {
        /// <summary>
        /// 使用者資訊
        /// </summary>
        public User? Info { get; set; }

        /// <summary>
        /// 過期時間
        /// </summary>
        public int ExpiresOn { get; set; }
    }
}
