﻿using BusinessLogic.Interfaces;
using BusinessLogic.Services;
using BusinessLogic.Services.U042;
using DataAccess.Interfaces;
using DataAccess.Repositorys;
using DataAccess.Services;
using Domain.Caches;
using Domain.Models.Security;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System.IO.Compression;
using System.Reflection;
using UtilityLib.Interfaces.Encryptions;
using UtilityLib.Interfaces.NLog;
using UtilityLib.Services.Encryptions;
using UtilityLib.Services.NLog;

namespace WebApp
{
    /// <summary>
    /// Startup
    /// </summary>
    public static class Startup
    {
        /// <summary>
        /// Configure The <see cref="WebApplicationBuilder"/>'s Services
        /// </summary>
        /// <param name="builder">The <see cref="WebApplicationBuilder"/></param>
        /// <returns>The <see cref="WebApplicationBuilder"/>.</returns>
        public static WebApplicationBuilder ConfigureServices(this WebApplicationBuilder builder)
        {
            IServiceCollection services = builder.Services;

            services.AddEndpointsApiExplorer();
            services.AddRazorPages();

            // 註冊Swagger
            services.AddSwaggerGen(c =>
            {
                // 文件描述
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "桃園水利處財務系統APIs",
                    Description = "因過多筆數會導致UI渲染嚴重延遲，經由SwaggerUI Request系統最多顯示10筆，實際操作不影響<br/>請先驗證(<strong>Authorize</strong>)後使用",
                });

                // Set the comments path for the Swagger JSON and UI. ( Set WebApp.csproj.PropertyGroup.GenerateDocumentationFile > true )
                // If response model is not under same project with generated xml for swagger, the <example> tag  won't work
                // more info: https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.0.3.md#runtimeExpression
                string[] xmlFiles = { Assembly.GetExecutingAssembly().GetName().Name!, "Domain" };
                xmlFiles.ToList().ForEach(xmlFile =>
                {
                    string xmlPath = Path.Combine(AppContext.BaseDirectory, $"{xmlFile}.xml");
                    c.IncludeXmlComments(xmlPath);
                });

                // Adding authorization to APIs
                //  1. https://github.com/domaindrivendev/Swashbuckle.AspNetCore#add-security-definitions-and-requirements
                c.AddSecurityDefinition("ApiKey", new OpenApiSecurityScheme()
                {
                    Type = SecuritySchemeType.ApiKey,
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Description = "<strong>APIs驗證安全令牌</strong><br/>請使用api/auth/generatetoken後<br/>在Header的Authorization中輸入",
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "ApiKey" }
                        },
                        new string[] { }
                    }
                });
            });

            // 註冊Dapper
            #region Database
            // cronjob中使用dapper 需改成單例模式，因為cronjob中會使用IHostedService，而IHostedService 必須是 Singleton
            // 但是 singleton 資料庫服務很危險(如果2000人上線其中一人request Dapper服務發生錯誤 其他1999人會無法使用)
            // 另外一個解法維持Scope型態，那CronJob實例類中，必需自建Scope 用完後Dispose
            // 參考: https://blog.darkthread.net/blog/aspnetcore-use-scoped-in-singleton/
            //// Scope
            services.AddScoped<IDapperRepository, DapperRepository>();
            services.AddScoped<IDapperService, DapperService>();
            #endregion

            // 註冊邏輯層服務
            #region BusinessLogic
            services.AddTransient<IHTTPStatusWrap, HTTPStatusWrapService>();
            services.AddScoped<ITokenManagerService, TokenManagerService>();
            services.AddTransient<AccountManagerService>();
            services.AddTransient<JRCCReferenceService>();
            services.AddTransient<U042S0010Service>();
            // 註冊BusinessLogic底下Validators
            services.AddFluentValidation(fv => fv.RegisterValidatorsFromAssembly(typeof(TokenManagerService).Assembly));
            #endregion

            // 註冊自定義工具
            #region UtilityLib
            services.AddTransient<IAesEncryptService, AesEncryptService>();
            services.AddTransient<INLogService, NLogService>();
            #endregion

            // 其餘服務: Session、封包壓縮、json null attribute處理
            #region Other Service

            // HTTPctx
            services.AddHttpContextAccessor();
            //Session
            services.AddSession();
            //封包壓縮
            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[]
                {
                    "text/plain", "text/css", "application/javascript", "text/html", "application/xml", "text/xml", "application/json", "text/json"
                });
                options.Providers.Add<GzipCompressionProvider>();
            });

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest;
            });

            //輸出Json時，不輸出null項目
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
            #endregion

            // 註冊參數
            #region AppDatCache
            services.Configure<ConnectionStrings>(builder.Configuration.GetSection("ConnectionStrings"));
            using (IServiceScope tmpScope = services.BuildServiceProvider().CreateScope())
            {
                IDapperRepository tmpDb = tmpScope.ServiceProvider.GetService<IDapperRepository>() !;
                EncryptKey appData = tmpDb.GetEncryptKey();
                // EncryptKey 放全域是否適合
                AppDataCache.AppEncryptKey = appData.App_encrypt_key;
            }
            #endregion

            return builder;
        }

        /// <summary>
        /// Configure The <see cref="WebApplication"/> Middlewares.
        /// The order is matter.
        /// </summary>
        /// <param name="app">The Given <see cref="WebApplication"/></param>
        /// <returns>The <see cref="WebApplication"/>.</returns>
        public static WebApplication ConfigureMiddleWare(this WebApplication app)
        {
            if (!!app.Environment.IsDevelopment() || !!app.Environment.IsStaging())
            {
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                    options.RoutePrefix = string.Empty;

                    // Custom js
                    options.InjectJavascript("js/swagger-ui.js");

                    // Custom css
                    options.InjectStylesheet("css/swagger-ui.css");

                    // custom Index.html (index.html屬性的建置動作需要先設為內嵌資源)
                    // 具體名字可以用 MethodBase.GetCurrentMethod().DeclaringType.Assembly.GetManifestResourceNames(); 內嵌資源列表查找
                    options.IndexStream = () => MethodBase.GetCurrentMethod() !.DeclaringType!.Assembly.GetManifestResourceStream("WebApp.wwwroot.index.html");

                    // 特別標註(檢查Reqest是否由SwaggerUI發出)
                    options.UseRequestInterceptor("(request) => { request.headers['swagger-ui']= true ; return request; }");

                    // 預設TryItOut已啟用
                    options.EnableTryItOutByDefault();
                });
            }

            // TODO: UseMiddleware
            // 接下來的MiddleWare 只要出錯就catch 出來
            // app.UseMiddleware<ExceptionMiddleware>();

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            // 啟用 Cookie 原則功能
            app.UseCookiePolicy();

            // 啟用身分識別
            app.UseAuthentication();

            // 啟用授權功能
            app.UseAuthorization();

            // 啟動 Session 功能
            app.UseSession();

            app.MapControllers();

            app.MapRazorPages();

            return app;
        }
    }
}
