﻿using DataAccess.Interfaces;
using DataAccess.Models;
using DataAccess.Repositorys.Base;
using Domain.Caches;
using Domain.Models.DataBase.JRCC;
using Domain.Models.Security;
using Microsoft.Extensions.Options;

namespace DataAccess.Repositorys
{
    /// <summary>
    /// DataAccess 多加一層Depth 彙總TSQL的存儲庫
    /// </summary>
    public class DapperRepository : BaseRepository, IDapperRepository
    {
        #region 注入/初始化

        /// <summary>
        /// 注入/初始化
        /// </summary>
        /// <param name="dapperService"></param>
        /// <param name="connectionStrings"></param>
        public DapperRepository(IDapperService dapperService, IOptions<ConnectionStrings> connectionStrings)
        {
            this.DapperService1 = dapperService;
            // this.SqlCommand = this.Configuration.GetConnectionString("SQLConnect");
            this.SqlCommand = connectionStrings.Value.SQLConnect;
        }

        #endregion

        #region 參考 jrcc

        /// <summary>
        /// [obsolete]
        /// jrcc0102
        /// 縣市資料
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public List<Jrcc0102_get> GetCityReference(string sqlCmd, object? sqlParams = null)
        {
            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = sqlCmd,
                SqlParamData = sqlParams
            };

            return this.DapperService1.Query<Jrcc0102_get>(dapperModel);
        }

        #endregion

        #region Account

        /// <summary>
        /// 取得使用者資料
        /// 如果沒有符合的資料返回null，符合多筆丟出錯誤
        /// </summary>
        /// <param name="whereClause"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public User Authenticate(string whereClause, object? sqlParams = null)
        {
            string sqlCmd = @"
                SELECT user_id as account, unit_id
                , CASE division_id WHEN '00' then 'C' /*本處*/ WHEN '99' then 'D' /*工作站*/ else 'B' /*承租人*/ end as user_type
                FROM ASYS0101";

            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = sqlCmd + whereClause,
                SqlParamData = sqlParams
            };

            return this.DapperService1.QuerySingleOrDefault<User>(dapperModel);
        }

        /// <summary>
        /// 回存Refresh Token
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public int StoreRefreshToken(string sqlCmd, object? sqlParams = null)
        {
            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = sqlCmd,
                SqlParamData = sqlParams
            };

            return this.DapperService1.Execute(dapperModel);
        }

        #endregion

        #region Security

        /// <summary>
        /// 取得APP的加密金鑰
        /// Key  Length: 76
        /// </summary>
        /// <returns></returns>
        public EncryptKey GetEncryptKey()
        {
            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = $@" SELECT App_encrypt_key FROM asys0000 ",
                SqlParamData = null
            };

            return this.DapperService1.QuerySingleOrDefault<EncryptKey>(dapperModel);
        }

        #endregion

        #region 共用

        /// <summary>
        /// SQL 查詢
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="sqlCmd"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public List<T> Query<T>(string sqlCmd, object? sqlParams = null)
        {
            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = sqlCmd,
                SqlParamData = sqlParams
            };

            return this.DapperService1.Query<T>(dapperModel);
        }

        /// <summary>
        /// SQL 執行
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public int Execute(string sqlCmd, object? sqlParams = null)
        {
            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = sqlCmd,
                SqlParamData = sqlParams
            };

            return this.DapperService1.Execute(dapperModel);
        }

        /// <summary>
        /// 取得第一筆資料
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="sqlCmd"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public T QueryFirstOrDefault<T>(string sqlCmd, object? query = null)
        {
            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = sqlCmd,
                SqlParamData = query
            };

            return this.DapperService1.QueryFirstOrDefault<T>(dapperModel);
        }

        /// <summary>
        /// 取得單一筆資料，有多筆符合條件會拋錯
        /// </summary>
        /// <typeparam name="T">物件模型</typeparam>
        /// <param name="sqlCmd"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public T QuerySingleOrDefault<T>(string sqlCmd, object? query = null)
        {
            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = sqlCmd,
                SqlParamData = query
            };

            return this.DapperService1.QuerySingleOrDefault<T>(dapperModel);
        }

        /// <summary>
        /// 共用方法: 新增資料
        /// </summary>
        /// <param name="tableName">資料表名稱</param>
        /// <param name="fields">欄位</param>
        /// <param name="values">欄位值</param>
        /// <param name="sqlParams">變數值</param>
        /// <returns>The infected Rows(<see cref="int"/>).</returns>
        public int InsertDB(string tableName, string fields, string values, object sqlParams)
        {
            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = @$"INSERT INTO {tableName}({fields}) VALUES({values})",
                SqlParamData = sqlParams
            };
            try
            {
               return this.DapperService1.Execute(dapperModel);
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine(e);
                return 0;
            }
        }

        /// <summary>
        /// 共用方法: 更新資料
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="updateCmds"></param>
        /// <param name="whereClause"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public int UpdateDB(string tableName, string updateCmds, string whereClause, object sqlParams)
        {
            DapperConnectModel dapperModel = new DapperConnectModel()
            {
                SqlConnectionString = this.SqlCommand,
                SqlCommand = @$"UPDATE {tableName} SET {updateCmds} {whereClause}",
                SqlParamData = sqlParams
            };
            try
            {
                return this.DapperService1.Execute(dapperModel);
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine(e);
                return 0;
            }
        }

        #endregion
    }
}
