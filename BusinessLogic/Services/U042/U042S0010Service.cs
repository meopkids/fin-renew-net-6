﻿using BusinessLogic.Services.Base;
using DataAccess.Interfaces;
using Domain.Models.DataBase.U042.S0010;
using Domain.Models.General;
using Domain.Models.DataBase.Enum;
using Microsoft.AspNetCore.Http;
using UtilityLib.Extensions;

namespace BusinessLogic.Services.U042
{
    /// <summary>
    /// 占用資料管理
    /// </summary>
    public class U042S0010Service : BaseExtraordinaryService
    {
        #region 引入其他服務
        #endregion

        #region 注入/初始化

        /// <summary>
        /// 注入/初始化
        /// </summary>
        /// <param name="dapperRepository"></param>
        /// <param name="httpContext"></param>
        public U042S0010Service(IDapperRepository dapperRepository, IHttpContextAccessor httpContext)
        {
            this.HttpContextAccessor = httpContext;
            this.DapperRepository = dapperRepository;
        }

        #endregion

        #region 公開方法

        /// <summary>
        /// 占用資料列表(查詢結果集)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public GeneralResponse GetListData(U042S0010_search query)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            DateTime currentTime = DateTime.Now;

            string whereClause = "";
            List<string> land042x_ids = new List<string>();

            // 前處理: 子表查詢條件
            string land0421_whereClause = this.AttributeToWhereClause(query, new string[] { "Idcard_no", "Name" });
            string land0422_whereClause = this.AttributeToWhereClause(query, new string[] { "Fictitious_id", "Use_money", "Pay_date_start", "Pay_date_end" });

            // 占用期間
            if (query.Ocupied_period.Bool())
            {
                switch (query.Ocupied_period)
                {
                    case OcupiedPeriodEnum.Y:
                        land0421_whereClause += $" AND occupy_period_end < '{currentTime.GetROCFormatDate()}' ";
                        break;
                    case OcupiedPeriodEnum.N:
                        land0421_whereClause += $" AND occupy_period_end >= '{currentTime.GetROCFormatDate()}' ";
                        break;
                    default:
                        break;
                }
            }

            // 土地類別
            if (query.Enterprise.Bool())
            {
                switch (query.Enterprise)
                {
                    case EnterpriseEnum.B:
                        whereClause += " AND files_no <> '' ";
                        break;
                    case EnterpriseEnum.N:
                        whereClause += " AND files_no = '' ";
                        break;
                    default:
                        break;
                }
            }

            // 繳納狀態
            if (query.Pay_status.Bool())
            {
                switch (query.Pay_status)
                {
                    case PayStatusEnum.Y:
                        land0422_whereClause += " AND pay_date <> '' ";
                        break;
                    case PayStatusEnum.N:
                        land0422_whereClause += $"  AND pay_date = '' AND clear_date = '' AND pay_date_end<>'' AND pay_date_end >= '{currentTime.GetROCFormatDate()}' ";
                        break;
                    case PayStatusEnum.O:
                        land0422_whereClause += $" AND pay_date_end < '{currentTime.GetROCFormatDate()}' AND pay_date = '' AND clear_date = '' ";
                        break;
                    default:
                        break;
                }
            }

            if (!string.IsNullOrEmpty(land0421_whereClause) || !string.IsNullOrEmpty(land0421_whereClause))
            {
                this.DapperRepository
                .Query<MyIdentity>(
                @$" SELECT Oid as id FROM land0421 WHERE 1=1 {(land0421_whereClause.Bool() ? land0421_whereClause : "and 1=0")}
                    UNION ALL 
                    SELECT Oid as id FROM land0422 WHERE 1=1 {(land0422_whereClause.Bool() ? land0422_whereClause : "and 1=0")}", query)
                    .AsEnumerable() // Linq must be IEnumerable
                   .GroupBy(x => x.Id)
                  .Select(g => g.First()) // Remove Repeated
                 .ToList() // Convert To List<string>
                .ForEach(i =>
                    {
                        land042x_ids.Add(i.Id.ToString());
                    });

                if (land042x_ids.Count > 0)
                {
                    string ids = $"'{string.Join("','", land042x_ids)}'";
                    whereClause += $" and land0420.id in ({ids})";
                }
            }

            // 主表查詢條件
            whereClause += this.AttributeToWhereClause(
                query,
                new string[] { "City_id", "Town_id", "Sec_id", "Files_no", "Land_no1_start", "Land_no1_end", "Land_no2_start", "Land_no2_end", "Division_id", "Unit_id" },
                "land0420");

            // 開始查詢
            string sqlCmd = @$"
                SELECT land0420.id, files_no, city_id, town_id, sec_id, land_no1, land_no2, land_category_name,land_area
                , jrcc0201.division_id, jrcc0202.unit_id, jrcc0201.division_name + '-' + jrcc0202.unit_name AS work_place
                , CASE WHEN m_update_id <> '' AND m_update_date <> '' THEN '*' ELSE '' END admin_checked		/* 管理員簽核 */
                , CASE WHEN s_update_id <> '' AND s_update_date <> '' THEN '*' ELSE '' END unitmanager_checked  /* 站長簽核 */
                , CASE WHEN l_update_id <> '' AND l_update_date <> '' THEN '*' ELSE '' END office_checked		/* 本處簽核 */
                , CASE ISNULL((SELECT SUM(1) FROM ottr1040 WHERE ottr1040.sid= land0420.id), '0') WHEN '0' THEN '' ELSE '*' END occupied_status  /* 占用狀態 */
                FROM land0420
                LEFT JOIN land0421 ON land0421.oid = land0420.id
                LEFT JOIN jrcc0106 ON land0420.land_category_id = jrcc0106.land_category_id
                LEFT JOIN jrcc0201 ON land0420.division_id = jrcc0201.division_id
                LEFT JOIN jrcc0202 ON land0420.division_id = jrcc0202.division_id AND land0420.unit_id = jrcc0202.unit_id
                WHERE land0420.types<>'P' {whereClause}";

            ret.Status = StatusCodes.Status200OK;
            ret.Row = this.DapperRepository.Query<U042S0010_list>(this.RowLIMTD(sqlCmd), query);

            return ret;
        }

        /// <summary>
        /// 占用資料查詢(land0420)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GeneralResponse GetLand0420Data(Guid id)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };

            string sqlCmd = "SELECT * FROM land0420 WHERE id = @Id";
            List<Land0420_get> results = this.DapperRepository.Query<Land0420_get>(this.RowLIMTD(sqlCmd), new { id });

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 新增占用資料(land0420)
        /// </summary>
        /// <param name="data"></param>
        /// <returns>The <see cref="GeneralResponse"/>.</returns>
        public GeneralResponse InsertMainData(Land0420_insert data)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };

            string whereClause = "";

            data.Land_no1 = data.Land_no1.Bool() ? data.Land_no1.ToString().PadLeft(4, '0') : data.Land_no1;
            data.Land_no2 = data.Land_no2.Bool() ? data.Land_no2.ToString().PadLeft(4, '0') : data.Land_no2;

            // 查詢條件分配
            whereClause += this.AttributeToWhereClause(data, new string[] { "City_id", "Town_id", "Sec_id", "Land_no1", "Land_no2" });

            // 舊地號
            MyIdentity refLand = this.DapperRepository.QuerySingleOrDefault<MyIdentity>(" SELECT id FROM land0100 WHERE 1=1 " + whereClause, data);

            if (!refLand.Bool())
            {
                ret.Row = "沒有這筆土地資料";
                return ret;
            }

            DateTime currentTime = DateTime.Now;

            object landUsageData = new
            {
                Id = Guid.NewGuid(),
                Lid = refLand.Id,
                Use_id = "240",
                Use_detail = "被侵佔地",
                Use_area = 0,
                Note = $"占用土地檔案編號：{data.Files_no}",
                Update_id = this.Payload!.Info!.Account,
                Update_date = currentTime.GetROCFormatDate(),
            };

            string[] sQLCmds = this.PropertiesToInsertCommands(landUsageData);
            int creatUsageRow = this.DapperRepository.InsertDB("land0120", sQLCmds[0], sQLCmds[1], landUsageData);

            // 新增占用資料
            if (creatUsageRow > 0)
            {
                object occupiedData = new
                {
                    Id = Guid.NewGuid(),
                    Files_no = data.Files_no,
                    Occupy_type = data.Occupy_type,
                    City_id = data.City_id,
                    Town_id = data.Town_id,
                    Sec_id = data.Sec_id,
                    Land_no1 = data.Land_no1,
                    Land_no2 = data.Land_no2,
                    Land_category_id = data.Land_category_id,
                    Land_kind = data.Land_kind,
                    Land_area = data.Land_area,
                    Land_use_id = data.Land_use_id,
                    Land_classification_id = data.Land_category_id,
                    P_bound_id = data.P_bound_id,
                    Division_id = data.Division_id,
                    Unit_id = data.Unit_id,
                    Update_id = this.Payload.Info.Account,
                    Update_date = currentTime.GetROCFormatDate()
                };

                string[] sQLCmds_2 = this.PropertiesToInsertCommands(occupiedData);

                int creatOccupiedRow = this.DapperRepository.InsertDB("land0420", sQLCmds_2[0], sQLCmds_2[1], occupiedData);

                if (creatOccupiedRow > 0)
                {
                    ret.Status = StatusCodes.Status200OK;
                    ret.Row = "新增成功";
                }
            }

            return ret;
        }

        /// <summary>
        /// 更新占用資料(land0420)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public GeneralResponse PutMainData(Guid id, Land0420_update data)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };

            // 原佔用資料，搬移至新地號中
            string sQLcmds_1 = @"
                SELECT land0100.id FROM land0100 
                INNER JOIN land0420 on land0420.city_id = land0100.city_id and land0420.town_id = land0100.town_id and land0420.sec_id = land0100.sec_id and land0420.land_no1 = land0100.land_no1 and land0420.land_no2 = land0100.land_no2
                WHERE land0420.id = @Id ";

            string sQLcmds_2 = @"
                SELECT id FROM land0100 
                WHERE city_id = @City_id and town_id = @Town_id and sec_id = @Sec_id and land_no1 = @Land_no1 and land_no2 = @Land_no2 "; // 新資料id

            MyIdentity originalLandData = this.DapperRepository.QuerySingleOrDefault<MyIdentity>(sQLcmds_1, new { Id = id.ToString() });

            if (originalLandData == null || originalLandData.Id == Guid.Empty)
            {
                ret.Row = "舊地號已遺失";
                return ret;
            }

            MyIdentity newLandData = this.DapperRepository.QuerySingleOrDefault<MyIdentity>(sQLcmds_2, data);

            if (newLandData.Id != originalLandData!.Id)
            {
                this.DapperRepository.UpdateDB("land0120", "lid = @Id", "WHERE id=@Id", newLandData);
            }

            // 更新占用資料
            string updateCmds_1 = @$"
                files_no=@Files_no, files_no_old=@Files_no_old
                ,city_id=@City_id, town_id=@Town_id, sec_id=@Sec_id, land_no1=@Land_no1, land_no2=@Land_no2
                ,update_id=@Account, update_date=@Update_date";

            DateTime currentTime = DateTime.Now;

            object newData = new
            {
                id,
                data.Files_no,
                data.Files_no_old,
                data.City_id,
                data.Town_id,
                data.Sec_id,
                data.Land_no1,
                data.Land_no2,
                data.Unit_id,
                data.Division_id,
                this.Payload!.Info!.Account,
                Update_date = currentTime.GetROCFormatDate()
            };

            int updatedRows = this.DapperRepository.UpdateDB("land0420", updateCmds_1, "WHERE id=@Id", newData);

            // 更新備註
            string updateCmds_2 = @$" 
                Declare @landstr nvarchar(50);
                SELECT @landstr = city_name + town_name + sec_name + '段' + 
                CASE sec_seq 
                    WHEN '' THEN '' 
                    ELSE sec_seq + '小段' END + 
                land_no1 + land_no2 + '號' 
                FROM land0420
                LEFT JOIN jrcc0102 on jrcc0102.city_id = land0420.city_id
                LEFT JOIN jrcc0103 on jrcc0103.city_id = land0420.city_id and jrcc0103.town_id = land0420.town_id
                LEFT JOIN jrcc0104 on jrcc0104.city_id = land0420.city_id and jrcc0104.town_id = land0420.town_id and jrcc0104.sec_id = land0420.sec_id 

                UPDATE land0421
                SET note = note + @landstr
                WHERE oid = @Id ";

            this.DapperRepository.Execute(updateCmds_2, new { id });

            ret.Status = StatusCodes.Status200OK;
            ret.Row = "已更新資料";

            return ret;
        }

        /// <summary>
        /// 刪除占用資料(land0420)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GeneralResponse DeleteMainData(Guid id)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };

            int deleteRows = this.DapperRepository.Execute(
                @"
                DELETE FROM land0422 WHERE oid=@Id
                DELETE FROM land0421 WHERE oid=@Id
                DELETE FROM land0420 WHERE id=@Id ",
                new { id });

            if (deleteRows > 0)
            {
                ret.Status = StatusCodes.Status204NoContent;
                ret.Row = "刪除成功";
            }

            return ret;
        }

        /// <summary>
        /// 占用調查記錄查詢(land0421)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GeneralResponse GetNavigData(Guid id)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };
            string sqlCmd = @"
                SELECT land0421.*, asys0101.name AS uname FROM land0421
                LEFT JOIN asys0101 ON asys0101.user_id = land0421.handle_id
                WHERE oid = @Oid";

            List<Land0421_get> results = this.DapperRepository.Query<Land0421_get>(this.RowLIMTD(sqlCmd), new { Oid = id.ToString() });

            ret.Status = StatusCodes.Status200OK;
            ret.Row = results;
            return ret;
        }

        /// <summary>
        /// 新增占用調查資料(land0421)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public GeneralResponse InsertNavigData(Land0421_insert data)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };

            DateTime currentTime = DateTime.Now;

            // 資料移轉: API param model 到真正要插入資料庫的資料還是有段差距，使用Display model物件複製大部分屬性值並微調小部分屬性值
            Land0421_get newData = data.AssignData(new Land0421_get()
            {
                Id = Guid.NewGuid(),
                Update_id = this.Payload!.Info!.Account,
                Update_date = currentTime.GetROCFormatDate()
            });

            // 資料新增: land0421
            string[] sQLCmds = this.PropertiesToInsertCommands(newData);
            int creatUsageRow = this.DapperRepository.InsertDB("land0421", sQLCmds[0], sQLCmds[1], newData);

            // 更新占用資料
            if (creatUsageRow > 0)
            {
                Land0420_get main_data = this.DapperRepository.QuerySingleOrDefault<Land0420_get>(
                    "SELECT * FROM land0420 WHERE id = @Oid", new { data.Oid });

                if (main_data == null)
                {
                    ret.Row = "占用主檔不存在";
                    return ret;
                }

                string sqlcmds = @"
                    SELECT id FROM land0100 
                    WHERE city_id=@City_id and town_id=@Town_id and sec_id=@Sec_id and land_no1=@Land_no1 and land_no2=@Land_no2
                ";

                MyIdentity strLandID = this.DapperRepository.QuerySingleOrDefault<MyIdentity>(sqlcmds, main_data);

                if (strLandID == null)
                {
                    ret.Row = "土地主檔沒有這筆土地資料";
                    return ret;
                }

                string updateCmds = "use_detail= CAST(use_detail AS nvarchar) + '-' + @Name, use_area = @Occupy_area";
                string whereClause = " WHERE lid=@Lid and use_area='0' and note like '%' + @Files_no + '%'";
                this.DapperRepository.UpdateDB("land0120", updateCmds, whereClause, new { Lid = strLandID.Id, main_data.Files_no, data.Name, data.Occupy_area });

                ret.Status = StatusCodes.Status200OK;
                ret.Row = "新增成功";
            }

            return ret;
        }

        /// <summary>
        /// 更新占用調查資料(land0421)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public GeneralResponse PutNaviData(Guid id, Land0421_update data)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };

            ret.Status = StatusCodes.Status200OK;
            ret.Row = "已更新資料";

            return ret;
        }

        /// <summary>
        /// 刪除占用調查資料(land0421)
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public GeneralResponse DeleteNavigData(Guid identity)
        {
            GeneralResponse ret = new GeneralResponse() { Status = StatusCodes.Status400BadRequest };

            int deleteRows = this.DapperRepository.Execute("DELETE FROM land0421 WHERE id=@Id ", new { identity });

            if (deleteRows > 0)
            {
                ret.Status = StatusCodes.Status200OK;
                ret.Row = "刪除成功";
            }

            return ret;
        }
        #endregion
    }
}
