﻿namespace Domain.Models.DataBase.JRCC
{
    /// <summary>
    /// 等則別查詢
    /// </summary>
    public class Jrcc0301_query
    {
        /// <summary>
        /// 等則Id
        /// </summary>
        public string? Land_kind { get; set; }

        /// <summary>
        /// 金額
        /// </summary>
        public double? Lease_money { get; set; }
    }
}
