﻿using BusinessLogic.Interfaces;
using Domain.Caches;
using Domain.Models.Security;
using System.Text;
using UtilityLib.Extensions;
using UtilityLib.Interfaces.Encryptions;

namespace BusinessLogic.Services
{
    /// <summary>
    /// 安全令牌管理服務
    /// Source: https://ithelp.ithome.com.tw/articles/10199102
    /// JsonWebToken 解決的是簽証(sign)安全，不是傳輸全安全，要配合加密通道(https)才能安全地傳遞 JWT
    /// </summary>
    public class TokenManagerService : ITokenManagerService
    {
        #region 引入服務

        /// <summary>
        /// AES加解密服務
        /// </summary>
        private readonly IAesEncryptService _aesEncryptService;

        /// <summary>
        /// 金鑰，從設定檔或資料庫取得
        /// </summary>
        private readonly string _key;

        /// <summary>
        /// 安全令牌持續時間(秒)
        /// </summary>
        private readonly int _duration;

        #endregion

        #region 注入/初始化

        /// <summary>
        /// 注入/初始化
        /// </summary>
        /// <param name="aesEncryptService"></param>
        public TokenManagerService(IAesEncryptService aesEncryptService)
        {
            this._aesEncryptService = aesEncryptService;
            this._key = AppDataCache.AppEncryptKey;
            this._duration = 3600;
        }

        #endregion

        #region 公開方法

        /// <summary>
        /// 產生 Token
        /// </summary>
        /// <param name="user">使用者</param>
        /// <returns></returns>
        public Token Create(User user)
        {
            Payload payload = new Payload
            {
                Info = user,
                // Unix 時間戳
                ExpiresOn = Convert.ToInt32(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds) + this._duration,
            };

            string json = payload.ToJson();
            string base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
            string iv = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
            string encrypt = this._aesEncryptService.EncryptData(base64, this._key.Substring(0, 16), iv);
            string signature = HmacSha256Extension.HMACSHA256(iv + "." + encrypt, this._key.Substring(0, 64));

            return new Token
            {
                AccessToken = iv + "." + encrypt + "." + signature,
                RefreshToken = Guid.NewGuid().ToString().Replace("-", ""),
            };
        }

        /// <summary>
        /// 驗證
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public AuthenticationResult Verify(string token)
        {
            AuthenticationResult result = new AuthenticationResult()
            {
                Status = ValidType.Success
            };

            try
            {
                string[] split = token.Split('.');
                string iv = split[0];
                string encrypt = split[1];
                string signature = split[2];

                if (signature != HmacSha256Extension.HMACSHA256(iv + "." + encrypt, this._key.Substring(0, 64)))
                {
                    result.Status = ValidType.Invalid;
                    return result;
                }

                string base64 = this._aesEncryptService.DecryptData(encrypt, this._key.Substring(0, 16), iv);
                string json = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
                Payload payload = json.ToModel<Payload>();

                if (payload.ExpiresOn < Convert.ToInt32(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds))
                {
                    result.Status = ValidType.Expired;
                    return result;
                }

                result.UserInfo = payload;
            }
            catch
            {
                result.Status = ValidType.Invalid;
                return result;
            }

            return result;
        }

        #endregion
    }
}
